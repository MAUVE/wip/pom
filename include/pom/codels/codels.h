/*                                                              -*-c++-*-
 * Copyright (c) 2015-2017 LAAS/CNRS
 * All rights reserved.
 *
 * Redistribution and use  in source  and binary  forms,  with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of  source  code must retain the  above copyright
 *      notice and this list of conditions.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice and  this list of  conditions in the  documentation and/or
 *      other materials provided with the distribution.
 *
 *                                      Anthony Mallet on Wed Sep 16 2015
 */
#ifndef H_POM_CODELS
#define H_POM_CODELS

#include <aio.h>

#include <map>

#include "pom_c_types.h"
#include "ukf.h"

struct pom_snapshot {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  bool fused;

  ukf::state_s state;
  ukf::state_s::dcov_s cov;

  or_pose_estimator_state measurement;
};

inline bool operator <(const or_time_ts &x, const or_time_ts &y)
{
  /* sorted by greater timestamps first */
  if (x.sec > y.sec) return true;
  if (x.sec == y.sec)
    if (x.nsec > y.nsec) return true;
  return false;
}

typedef std::multimap<
  or_time_ts, pom_snapshot,
  std::less<or_time_ts>,
  Eigen::aligned_allocator<
    std::pair<const or_time_ts, pom_snapshot> > > pom_history_s;

struct pom_context_s {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  or_time_ts ts;
  ukf::state_s state;
  ukf::state_s::dcov_s cov;

  ukf::filter_s filter;
  pom_history_s history;
};

struct pom_log_s {
  struct aiocb req;
  char buffer[4096];
  bool pending, skipped;
  uint32_t decimation;
  size_t missed, total;

# define pom_logfmt	" %g"
# define pom_log_header_fmt                                             \
  "ts i pp vp ap x y z roll pitch yaw vx vy vz wx wy wz ax ay az"
# define pom_log_state_fmt                                              \
  "%d.%09d %d %d %d %d"                                                 \
  pom_logfmt pom_logfmt pom_logfmt pom_logfmt pom_logfmt pom_logfmt     \
  pom_logfmt pom_logfmt pom_logfmt pom_logfmt pom_logfmt pom_logfmt     \
  pom_logfmt pom_logfmt pom_logfmt
# define pom_scan_state_fmt                                             \
  "%d.%09d %d %d %d %d"                                                 \
  "%lf %lf %lf %lf %lf %lf"                                             \
  "%lf %lf %lf %lf %lf %lf"                                             \
  "%lf %lf %lf"
};


static inline genom_event
pom_e_sys_error(const char *s, genom_context self)
{
  pom_e_sys_detail d;
  size_t l = 0;

  d.code = errno;
  if (s) {
    strncpy(d.what, s, sizeof(d.what) - 3);
    l = strlen(s);
    strcpy(d.what + l, ": ");
    l += 2;
  }
  if (strerror_r(d.code, d.what + l, sizeof(d.what) - l))
    /* ignore error (BSD) or return value (Linux) */;
  return pom_e_sys(&d, self);
}

typedef sequence<pom_portinfo_s> sequence_pom_portinfo_s;
typedef sequence<or_pose_estimator_state> sequence_or_pose_estimator_state;
/* IO */
genom_event pom_io_start(pom_ids*, const genom_context);
genom_event pom_io_read(const pom_measure*, sequence_pom_portinfo_s*,
  sequence_or_pose_estimator_state*, const genom_context);
genom_event pom_io_insert(sequence<or_pose_estimator_state> *,
  pom_context_s **, uint16_t, pom_log_s **, const genom_context);
genom_event pom_io_stop(pom_ids *, const genom_context);
genom_event pom_add_measurement(const char[128], sequence_pom_portinfo_s *,
  sequence_or_pose_estimator_state *, const genom_context);
genom_event pom_replay_start(const char[128], const genom_context);
genom_event pom_replay_read(sequence_or_pose_estimator_state *, const genom_context);
genom_event pom_replay_stop(const genom_context);
/* LOG */
genom_event pom_log_stop(pom_log_s **, pom_log_s **, const genom_context);
/* FILTER */
genom_event pom_filter_start(pom_context_s **, const pom_frame *, const genom_context);
genom_event pom_filter_exec(pom_context_s **, double, const pom_ids_offset_s *, pom_log_s **, const pom_frame *, const genom_context);
/* CODELS */
genom_event set_process_noise(double, double, const genom_context);
genom_event pom_set_position(double x, double y, double z, double roll,
                 double pitch, double yaw,
                 const pom_context_s *context,
                 pom_ids_offset_s *offset, const genom_context);

#endif /* H_POM_CODELS */
