#ifndef H_POM_C_TYPES
#define H_POM_C_TYPES

#include <stdint.h>
#include <vector>
#include <string.h>
#include <stdlib.h>

struct genom_context {
  genom_context() {};
};

enum genom_event {
  genom_ok,
  pom_exec,
  pom_pause_exec,
  pom_read,
  pom_pause_read,
  pom_insert,
  pom_ether,
  pom_stop,
  pom_error
};

struct pom_e_sys_detail {
  int16_t code;
  char what[128];
};
inline genom_event pom_e_sys(pom_e_sys_detail* detail, genom_context self) { return pom_error; };

template <typename T>
 struct optional {
   // types:
   typedef T                                     value_type;
   typedef value_type&                           reference;
   typedef const value_type&                     const_reference;

   bool _present;
   value_type _value;
 };

struct or_time_ts {
  uint32_t sec;	/* seconds */
  uint32_t nsec;	/* nanoseconds */
};

/* position */
struct or_t3d_pos {
  double x, y, z;		/* X, Y, Z translations */
  double qw, qx, qy, qz;	/* quaternion orientation */
};

struct or_t3d_pos_cov {
  double cov[28];	/* covariance matrix on translations and rotations
                     * (lower triangular row major) */
};

/* velocity */
struct or_t3d_vel {
  double vx, vy, vz;	/* linear */
  double wx, wy, wz;	/* angular */
};

struct or_t3d_vel_cov {
  double cov[21];	/* covariance matrix on v and w
                     * (lower triangular row major) */
};

/* acceleration */
struct or_t3d_acc {
  double ax, ay, az;	/* linear */
};

struct or_t3d_acc_cov {
  double cov[6];	/* covariance matrix on a
                     * (lower triangular row major) */
};

struct or_pose_estimator_state {
  or_time_ts ts;	/* timestamp */
  bool intrinsic;	/* velocity and acceleration in cartesian (base frame)
                       * coordinates (false), or intrinsic coordinates (local
                       * frame) (true) */

  optional<or_t3d_pos> pos;		/* position */
  optional<or_t3d_pos_cov> pos_cov;	/* position uncertainty */

  optional<or_t3d_vel> vel;		/* velocity */
  optional<or_t3d_vel_cov> vel_cov;	/* velocity uncertainty */

  optional<or_t3d_acc> acc;		/* acceleration */
  optional<or_t3d_acc_cov> acc_cov;	/* acceleration uncertainty */
};

/* frame offset */
struct pom_ids_offset_s {
  double p[3], q[4];
};

struct pom_frame {
  or_pose_estimator_state* _data;
  void open(const char*, genom_context) const {};
  void write(const char*, genom_context) const {};
  or_pose_estimator_state* data(const char*, genom_context) const { return _data; };
};

#include <map>
#include <memory>
struct pom_measure {
  struct cmp_str {
    bool operator()(const char* a, const char* b) const {
      return strcmp(a, b) < 0;
    }
  };
  std::map<const char*, std::shared_ptr<or_pose_estimator_state>, cmp_str> _data;
  void open(const char*, genom_context) const {};
  genom_event read(const char*, genom_context) const { return genom_ok; };
  or_pose_estimator_state* data(const char* name, genom_context) const { return _data.at(name).get(); };
};

struct pom_context_s;
struct pom_log_s;

struct pom_portinfo_s {
  char name[128];
  or_time_ts last;
};

template <typename T>
struct sequence {
  std::vector<T> _buffer;
  char name[128];
  inline uint32_t maximum() { return _buffer.size(); };
  uint32_t _length = 0;
  inline uint32_t& length() { return _length; };
};

template <typename T>
bool genom_sequence_reserve(sequence<T>* seq, uint32_t length) {
  seq->_buffer.resize(length);
  return false;
};

struct pom_ids {
  pom_context_s* context;
  uint16_t history_length;

  sequence<or_pose_estimator_state> measurements;
  sequence<pom_portinfo_s> ports;

  /* frame offset */
  pom_ids_offset_s offset;

  /* filter parameters */
  double max_jerk, max_dw;

  /* logging */
  pom_log_s *log_state, *log_measurements;
};

#endif
