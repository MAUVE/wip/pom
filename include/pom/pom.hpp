#include <mutex>
#include <thread>

#include "codels/codels.h"

class POM {
  using or_pose_estimator_state_ptr = std::shared_ptr<or_pose_estimator_state>;
private:
  bool restamp;
  double restamp_offset;

  std::mutex history_mutex;
  bool stop_requested = false;

  pom_measure measure;

protected:
  pom_frame frame;
  const genom_context self;
  double pom_period_ms = 100;
  bool use_odom_linear, use_odom_angular;
  bool use_imu_orientation, use_imu_velocity, use_imu_acceleration;
  bool use_gps;

  or_t3d_vel* odomCov = new or_t3d_vel;
  or_t3d_vel* gyroCov = new or_t3d_vel;
  or_t3d_acc* accCov = new or_t3d_acc;

  or_t3d_pos* imuPoseBias = new or_t3d_pos;
  or_t3d_vel* imuVelBias = new or_t3d_vel;
  or_t3d_acc* imuAccBias = new or_t3d_acc;
  or_t3d_vel* odomVelBias = new or_t3d_vel;
  or_t3d_pos* gpsPoseBias = new or_t3d_pos;

  void addMeasure(const std::string& data, or_pose_estimator_state_ptr& m);

public:
  pom_ids ids;

  void request_stop();
  void stop();
  bool start();
  virtual void publishEstimate();

  void io();
  void filter();

  void set_position(double x, double y, double z, double roll, double pitch, double yaw);
  void set_process_noise(double dadt, double dwdt);
  void set_reference_frame(double x, double y, double z, double roll, double pitch, double yaw);

  POM(bool restamp);

  void add_imu(bool use_orientation, bool use_velocity, bool use_acceleration);
  void add_odom(bool use_linear, bool use_angular);
  void add_gps();

};
