#ifndef H_POM_CONVERSIONS
#define H_POM_CONVERSIONS

#include "codels.h"
#include "conversions.h"
#include <sys/time.h>

void mult(or_t3d_pos& q1, or_t3d_pos& q2) {
  double w = q1.qw * q2.qw - q1.qx * q2.qx - q1.qy * q2.qy - q1.qz * q2.qz;
  double x = q1.qw * q2.qx + q1.qx * q2.qw + q1.qy * q2.qz - q1.qz * q2.qy;
  double y = q1.qw * q2.qy + q1.qy * q2.qw + q1.qz * q2.qx - q1.qx * q2.qz;
  double z = q1.qw * q2.qz + q1.qz * q2.qw + q1.qx * q2.qy - q1.qy * q2.qx;
  q1.qw = w;
  q1.qx = x;
  q1.qy = y;
  q1.qz = z;
}

void conjugate(or_t3d_pos& q) {
  q.qw = q.qw;
  q.qx = -q.qx;
  q.qy = -q.qy;
  q.qz = -q.qz;
}

bool restamp(std::shared_ptr<or_pose_estimator_state> m, double* offset) {
  struct timeval now;
  gettimeofday(&now, NULL);
  if (std::isnan(*offset)) {
    *offset = now.tv_sec + 1e-6*now.tv_usec - m->ts.sec - 1e-9*m->ts.nsec;
    std::cout << "restamp offset: " << *offset << std::endl;
  }
  double t = m->ts.sec + 1e-9*m->ts.nsec + (*offset);
  m->ts.sec = floor(t);
  m->ts.nsec = 1e9*(t - m->ts.sec);
  return true;
}

bool timestamp(std::shared_ptr<or_pose_estimator_state> m) {
  struct timeval now;
  gettimeofday(&now, NULL);
  m->ts.sec = now.tv_sec;
  m->ts.nsec = 1000*now.tv_usec;
  return true;
}

#endif // H_POM_CONVERSIONS
