#ifndef H_POM_ROS_CONVERSIONS
#define H_POM_ROS_CONVERSIONS

#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include "pom_conversions.hpp"

bool GetOdometry(
  const nav_msgs::Odometry* OdometryData,
  std::shared_ptr<or_pose_estimator_state> m,
  bool use_linear, bool use_angular,
  or_t3d_vel* odomCov, or_t3d_vel* odometryBias)
{
  timestamp(m);
  m->intrinsic = true;        // This is just the pure odometry (speed)
  auto twist = OdometryData->twist.twist;

  if (use_linear) {
    m->vel._present = true,
    m->vel._value.vx = twist.linear.x - odometryBias->vx;
    m->vel._value.vy = twist.linear.y - odometryBias->vy;
    m->vel._value.vz = twist.linear.z - odometryBias->vz;
  } else {
    m->vel._value.vx = nan("");
    m->vel._value.vy = nan("");
    m->vel._value.vz = nan("");
  }

  if (use_angular) {
    m->vel._present = true,
    m->vel._value.wx = twist.angular.x - odometryBias->wx;
    m->vel._value.wy = twist.angular.y - odometryBias->wy;
    m->vel._value.wz = twist.angular.z - odometryBias->wz;
  } else {
    m->vel._value.wx = nan("");
    m->vel._value.wy = nan("");
    m->vel._value.wz = nan("");
  }

  m->vel_cov = {
    ._present = true,
    ._value = {{
      odomCov->vx,
      0.0, odomCov->vy,
      0.0, 0.0, odomCov->vz,
      0.0, 0.0, 0.0, odomCov->wx,
      0.0, 0.0, 0.0, 0.0, odomCov->wy,
      0.0, 0.0, 0.0, 0.0, 0.0, odomCov->wz,
    }}
  };
  m->pos._present =
  m->acc._present =
  m->pos_cov._present =
  m->acc_cov._present = false;

  return true;
}

bool GetImu(const sensor_msgs::Imu* id,
      std::shared_ptr<or_pose_estimator_state> ImuPoseData,
      bool use_orientation, bool use_velocity, bool use_acceleration,
      or_t3d_pos* imuPoseBias, or_t3d_vel* imuVelBias, or_t3d_acc* imuAccBias,
      or_t3d_vel* gyroCov, or_t3d_acc* accCov)
{
  timestamp(ImuPoseData);

  double qw = id->orientation.w;
  double qx = id->orientation.x;
  double qy = id->orientation.y;
  double qz = id->orientation.z;

  ImuPoseData->intrinsic = true;

  if (use_orientation) {
    ImuPoseData->pos = {
      ._present = false,
      ._value = {
        .x = nan(""),
        .y = nan(""),
        .z = nan(""),
        .qw = id->orientation.w,
        .qx = id->orientation.x,
        .qy = id->orientation.y,
        .qz = id->orientation.z
      }
    };
    if (imuPoseBias) {
      conjugate(*imuPoseBias);
      mult(ImuPoseData->pos._value, *imuPoseBias);
    }
    ImuPoseData->pos_cov._present = false;
  }

  if (use_velocity) {
    ImuPoseData->vel = {
      ._present = true,
      ._value = {
        .vx = nan(""),
        .vy = nan(""),
        .vz = nan(""),
        .wx = id->angular_velocity.x - imuVelBias->wx,
	      .wy = id->angular_velocity.y - imuVelBias->wy,
	      .wz = id->angular_velocity.z - imuVelBias->wz
      }
    };
    ImuPoseData->vel_cov = {
      ._present = true,
      ._value = {{
          gyroCov->vx,
          0., gyroCov->vy,
          0., 0., gyroCov->vz,
          0., 0., 0., gyroCov->wx,
          0., 0., 0., 0., gyroCov->wy,
          0., 0., 0., 0., 0., gyroCov->wz
      }}
    };
  }

  if (use_acceleration) {
    ImuPoseData->acc = {
      ._present = false,
      ._value = {
        .ax = id->linear_acceleration.x - imuAccBias->ax,
	      .ay = id->linear_acceleration.y - imuAccBias->ay,
	      .az = id->linear_acceleration.z - imuAccBias->az
      }
    };
    ImuPoseData->acc_cov = {
      ._present = false,
      ._value = {{
	       accCov->ax,
	       0., accCov->ay,
	       0.,   0.,  accCov->az
      }}
    };
  }

  return true;
}

bool GetNavSatFix(
  const sensor_msgs::NavSatFix* FixData,
  std::shared_ptr<or_pose_estimator_state> PoseData,
  or_t3d_pos* gpsPoseBias)
{
  timestamp(PoseData);

  double x, y;
  char zone;
  gps_common::LLtoUTM(FixData->latitude, FixData->longitude, y, x, &zone);
  auto& C = FixData->position_covariance;

  PoseData->intrinsic = false;
  PoseData->pos = {
    ._present = true,
    ._value = {
      .x = x - gpsPoseBias->x,
      .y = y - gpsPoseBias->y,
      .z = FixData->altitude - gpsPoseBias->z,
      .qw = nan(""),
      .qx = nan(""),
      .qy = nan(""),
      .qz = nan("")
    }
  };
  PoseData->pos_cov = {
    ._present = true,
    ._value = {{
      C[0],
      C[3], C[4],
      C[6], C[7], C[8],
      0., 0., 0., 0.,
      0., 0., 0., 0., 0.,
      0., 0., 0., 0., 0., 0.
    }}
  };

  PoseData->vel._present = PoseData->vel_cov._present = false;
  PoseData->acc._present = PoseData->acc_cov._present = false;
  return true;
}

void GetPose(const or_pose_estimator_state* m,
  geometry_msgs::PoseStamped& p)
{
  p.header.stamp.sec = m->ts.sec;
  p.header.stamp.nsec = m->ts.nsec;
  p.header.frame_id = "world";
  p.pose.position.x = m->pos._value.x;
  p.pose.position.y = m->pos._value.y;
  p.pose.position.z = m->pos._value.z;
  p.pose.orientation.x = m->pos._value.qx;
  p.pose.orientation.y = m->pos._value.qy;
  p.pose.orientation.z = m->pos._value.qz;
  p.pose.orientation.w = m->pos._value.qw;
}

void GetTwist(const or_pose_estimator_state* m,
  geometry_msgs::TwistStamped& t)
{
  t.header.stamp.sec = m->ts.sec;
  t.header.stamp.nsec = m->ts.nsec;
  t.header.frame_id = "robot";
  t.twist.linear.x = m->vel._value.vx;
  t.twist.linear.y = m->vel._value.vy;
  t.twist.linear.z = m->vel._value.vz;
  t.twist.angular.x = m->vel._value.wx;
  t.twist.angular.y = m->vel._value.wy;
  t.twist.angular.z = m->vel._value.wz;
}

#endif // H_POM_ROS_CONVERSIONS
