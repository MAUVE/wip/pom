#!/usr/bin/env python

from utmproj import *
from geometry import *
from estimate_init import plot_regression, plot_estimate
import matplotlib.pyplot as plt

import rospy
from sensor_msgs.msg import NavSatFix
from std_srvs.srv import Trigger, TriggerResponse

class HeadingEstimator:
	def __init__(self):
		self.origin = None
		self.heading = None
		self.positions = []
		self.rtk_positions = []

	def gpsCallback(self, msg, pos):
		if self.origin is None:
			self.origin, self.proj = init_utm(msg.latitude, msg.longitude, msg.altitude)
		x, y, z = convert(msg.latitude, msg.longitude, msg.altitude, self.origin, self.proj)
		pos.append([x, y, z])

	def estimateCallback(self, req):
		if self.positions:
			self.x = [x for x, _, _ in self.positions]
			self.y = [y for _, y, _ in self.positions]
			self.gradient, self.intercept, _, _, _ = linregress(self.x, self.y)
			self.heading = heading(self.gradient, self.x[-1] - self.x[0], self.y[-1] - self.y[0])
			#print("heading estimation: {}, {}rad, {}deg".format(self.gradient, self.heading, np.rad2deg(self.heading)))
			#print("final position: {}, {}, {}".format(self.positions[-1][0], self.positions[-1][1], self.positions[-1][2]))
			#print("final RTK position: {}, {}, {}".format(self.rtk_positions[-1][0], self.rtk_positions[-1][1], self.rtk_positions[-1][2]))
			return TriggerResponse(True, str(self.heading))
		else:
			return TriggerResponse(False, "no position received")

if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Estimate heading')
	parser.add_argument('--plot', '-p', action='store_true')
	parser.add_argument('--reference', nargs='*', type=float)
	args = parser.parse_args(rospy.myargv()[1:])

	est = HeadingEstimator()

	if args.reference:
		lat, lon, alt = args.reference
		est.origin, est.proj = init_utm(lat, lon, alt)

	rospy.init_node("estimate_heading")
	rospy.Subscriber("gps_fix", NavSatFix, lambda msg: est.gpsCallback(msg, est.positions))
	rospy.Subscriber("rtk_fix", NavSatFix, lambda msg: est.gpsCallback(msg, est.rtk_positions))
	rospy.Service('estimate', Trigger, est.estimateCallback)
	rospy.spin()

	try:
		if est.heading == None:
			est.estimateCallback(None)

		if args.plot:
			plt.figure("Heading estimate")
			plot_regression(est.x, est.y, est.gradient, est.intercept, "gps")
			plt.tight_layout()
			plt.axes().set_aspect('equal', 'datalim')
			plt.show()

		print("### Heading estimation ###")
		print("heading: {}".format(est.heading))
		o = est.origin
		print("origin: [{}, {}, {}]".format(o[0], o[1], o[2]))
		p = est.positions[-1]
		print("gps_bias: [{}, {}, {}]".format(p[0]+o[0], p[1]+o[1], p[2]+o[2]))
		print("reference_frame: [{}, {}, {}]".format(p[0], p[1], p[2]))
		p = est.rtk_positions[-1]
		print("reference_rtk_frame: [{}, {}, {}]".format(p[0], p[1], p[2]))

	except AttributeError:
		pass
