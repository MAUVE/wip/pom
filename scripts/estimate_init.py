#!/usr/bin/env python

from parser import parse_bag, get_rate
from utmproj import *
from geometry import *

import matplotlib.pyplot as plt

def plot_regression(x, y, slope, intercept, name):
	mn = np.min(x)
	mx = np.max(x)
	x1 = np.linspace(mn, mx, 500)
	y1 = slope * x1 + intercept
	plt.plot(x, y, 'ob')
	plt.plot(x1, y1, '-r')
	plt.legend(['data', 'regression'])
	plt.grid('on')
	plt.title(name)

def plot_estimate(x, y, mean, name):
	mn = np.min(x)
	mx = np.max(x)
	x1 = np.linspace(mn, mx, 500)
	y1 = np.full(len(x1), mean)
	plt.plot(x, y, 'ob')
	plt.plot(x1, y1, '-r')
	plt.legend(['data', 'estimate'])
	plt.grid('on')
	plt.title(name)

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description='Estimate initial heading and bias from bag')
	parser.add_argument('bag', type=str, help='ROS bag with initial data')
	parser.add_argument('--yaml', '-y', type=str, help='output estimates to ROS param file')
	parser.add_argument('--plot', action='store_true')
	parser.add_argument('--fixduration', '-d', type=float, help='duration of fix', default=1.0)
	parser.add_argument('--odom', '-o', action='store_true', help="analyse Odometry data")
	parser.add_argument('--imu', '-i', action='store_true', help="analyse IMU data")
	parser.add_argument('--gnss', '-g', action='store_true', help="analyse GNSS data")
	args = parser.parse_args()
	bag = args.bag

	if args.yaml:
		fileout = open(args.yaml, 'w')

	## GNSS
	if args.gnss:
		fixes = parse_bag(bag, topic="/ublox/fix")
		orig, proj = init_utm(fixes[0][1], fixes[0][2])

		xys = [convert(lat, lon, orig, proj) for _, lat, lon, _ in fixes]
		x = [x for x, _ in xys]
		y = [y for _, y in xys]
		gradient, intercept, _, _, _ = linregress(x, y)
		h = heading(gradient, x[-1] - x[0], y[-1] - y[0])
		print("heading estimation from ublox: {}, {}rad, {}deg".format(gradient, h, np.rad2deg(h)))
		if args.yaml:
			fileout.write("heading: {}\n".format(h))

		#mean, stdev = estimate(x)
		#print("estimate of GPS x: {} (stdev: {} cov: {})".format(
		#	mean, stdev, (stdev*stdev)))
		#mean, stdev = estimate(y)
		#print("estimate of GPS y: {} (stdev: {} cov: {})".format(
		#	mean, stdev, (stdev*stdev)))

		fixes = parse_bag(bag, topic='/ublox_rtk/fix')
		xys = [convert(lat, lon, orig, proj) for _, lat, lon, _ in fixes]
		x = [x for x, _ in xys]
		y = [y for _, y in xys]
		gradient, _, _, _, _ = linregress(x, y)
		rtk_h = heading(gradient, x[-1] - x[0], y[-1] - y[0])
		print("heading estimation from RTK: {}, {}rad, {}deg".format(gradient, rtk_h, np.rad2deg(rtk_h)))

		if args.yaml:
			fileout.write("gps_bias: [{}, {}, {}]\n".format(orig[0], orig[1], fixes[0][3]))

		if args.plot:
			plt.figure("Heading estimate")
			plt.subplot(211)
			plot_regression(x, y, gradient, intercept, "ublox")
			plt.subplot(212)
			plot_regression(x, y, gradient, intercept, "rtk")
			plt.tight_layout()
			plt.show()

	## ODOM
	if args.odom:
		odom = parse_bag(bag, topic="/summit_xl/odom")
		r = int( args.fixduration * get_rate(bag, "/summit_xl/odom") )
		labels = ['time', 'vx', 'vy', 'vz', 'wx', 'wy', 'wz']
		for i in range(1,7):
			mean, stdev = estimate(odom[:r,6+i])
			print("estimate of Odom {}: {} (stdev: {} cov: {})".format(
				labels[i], mean, stdev, (stdev*stdev)))

	## IMU
	if args.imu:
		imu = parse_bag(bag, topic="/xsens/imu_data")
		r = int( args.fixduration * get_rate(bag, "/xsens/imu_data") )
		if args.plot: plt.figure("IMU")
		labels = ['time', 'roll', 'pitch', 'yaw', 'wx', 'wy', 'wz', 'ax', 'ay', 'az']
		bias = {k: 0.0 for k in labels[1:]}
		for i in range(1,10):
			if args.plot: plt.subplot(330+i)
			mean, stdev = estimate(imu[:r,i])
			print("estimate of IMU {}: {} (stdev: {} cov: {})".format(
				labels[i], mean, stdev, (stdev*stdev)))
			if args.plot: plot_estimate(imu[:r,0], imu[:r,i], mean, labels[i])
			bias[labels[i]] = mean
		if args.plot: plt.tight_layout()
		if args.plot: plt.show()

		if args.yaml:
 			fileout.write("imu_acc_bias: [{}, {}, {}]\n".format(bias['ax'], bias['ay'], bias['az']))
 			fileout.write("imu_vel_bias: [{}, {}, {}]\n".format(bias['wx'], bias['wy'], bias['wz']))
			r = bias['roll']
			p = bias['pitch']
			y = bias['yaw'] - (h if h else 0)
			qx, qy, qz, qw = quaternion_from_euler(r, p, y)
			fileout.write("imu_att_bias: [{}, {}, {}, {}]\n".format(qx, qy, qz, qw))

	## END
	if args.yaml:
		fileout.close()
