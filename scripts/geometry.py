#!/usr/bin/env python
import numpy as np
import scipy.stats as stats

from tf.transformations import euler_from_quaternion, quaternion_from_euler

def linregress(x, y):
	return stats.linregress(x, y)

def estimate(x):
	return np.mean(x), np.std(x)

def heading(gradient, dx, dy):
	h_d = np.arctan2(dy, dx)
	h = np.arctan(gradient)
	if h * h_d > 0:
		return h
	else:
		return np.pi - h

if __name__ == '__main__':
    a = np.linspace(0, 2*np.pi)
    x = np.cos(a)
    y = np.sin(a)
    for i in range(a.size):
        gradient, intercept, _, _, _ = linregress([0,x[i]], [0,y[i]])
        h = heading(gradient, x[i], y[i])
        t = [h, 2*np.pi-h, 2*np.pi+h]
        print("{}\t{}\t{}\t{}\t{}".format((a[i] in t), np.rad2deg(a[i]), np.rad2deg(h), np.rad2deg(2*np.pi-h), np.rad2deg(2*np.pi+h)))
