#!/usr/bin/env python

import rosbag
import rospy
import numpy as np
from tf.transformations import euler_from_quaternion

def parse_odom(stamp, msg):
	p = msg.pose.pose.position
	q = msg.pose.pose.orientation
	v = msg.twist.twist.linear
	w = msg.twist.twist.angular
	r, g, y = euler_from_quaternion((q.x, q.y, q.z, q.w))
	return [stamp.to_sec(), \
		p.x, p.y, p.z, r, g, y, \
		v.x, v.y, v.z, w.x, w.y, w.z]

def parse_twist(stamp, msg):
	v = msg.twist.linear
	w = msg.twist.angular
	return [stamp.to_sec(),
		v.x, v.y, v.z, w.x, w.y, w.z]

def parse_imu(stamp, msg):
    qx = msg.orientation.x
    qy = msg.orientation.y
    qz = msg.orientation.z
    qw = msg.orientation.w
    r, p, y = euler_from_quaternion((qx, qy, qz, qw))
    return [stamp.to_sec(), r, p, y,
        msg.angular_velocity.x, msg.angular_velocity.y, msg.angular_velocity.z,
        msg.linear_acceleration.x, msg.linear_acceleration.y, msg.linear_acceleration.z]

def parse_fix(stamp, msg):
    return [stamp.to_sec(), msg.latitude, msg.longitude, msg.altitude]

def parse_pose(stamp, msg):
    qx = msg.pose.orientation.x
    qy = msg.pose.orientation.y
    qz = msg.pose.orientation.z
    qw = msg.pose.orientation.w
    pos = msg.pose.position
    r, p, y = euler_from_quaternion((qx, qy, qz, qw))
    return [stamp.to_sec(), pos.x, pos.y, pos.z, r, p, y]

msg_parsers = {
	'nav_msgs/Odometry': parse_odom,
	'sensor_msgs/Imu': parse_imu,
	'sensor_msgs/NavSatFix': parse_fix,
	'geometry_msgs/PoseStamped': parse_pose,
	'geometry_msgs/TwistStamped': parse_twist
}

def parse_bag(bagfile, topic, duration=None):
	data = []
	bag = rosbag.Bag(bagfile)
	s = rospy.Time.from_sec(bag.get_start_time())
	e = (None if duration is None else s + rospy.Duration(duration))
	for to, msg, ti in bag.read_messages(topics=topic, end_time=e):
		data.append(msg_parsers[msg._type](ti, msg))
	return np.array(data)

def get_rate(bagfile, topic):
	bag = rosbag.Bag(bagfile)
	ts = bag.get_start_time()
	tf = bag.get_end_time()
	n = bag.get_message_count(topic)
	return n / (tf - ts)
