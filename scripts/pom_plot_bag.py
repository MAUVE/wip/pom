#!/usr/bin/env python
import sys
import math
import time
import matplotlib.pyplot as plt
import numpy as np

from utmproj import init_utm, convert
from parser import parse_bag

def plot_gnss(gnss, rtk, name, t0=None):
    if t0 is None:
        t0 = gnss[0,0]

    plt.figure(name)

    for i, x in [(1,'X'), (2,'Y'), (3,'Z')]:
        plt.subplot(310+i)
        plt.plot(gnss[:,0]-t0, gnss[:,i])
        plt.plot(rtk[:,0]-t0, rtk[:,i])
        plt.legend(['GNSS','RTK'])
        plt.grid('on')
        plt.xlabel('Time (s)')
        plt.ylabel('Coordinates (m)')
        plt.title("{} coordinate".format(x))

    plt.tight_layout()
    return t0

def plot_pom(pose, vel, name, t0=None):
    if t0 is None:
        t0 = pose[0,0]

    plt.figure(name)
    plt.subplot(221)
    plt.plot(pose[:,0]-t0, pose[:,1:4])
    plt.legend(['X','Y','Z'])
    plt.grid('on')
    plt.xlabel('Time (s)')
    plt.ylabel('Coordinates (m)')
    plt.title('Position')

    plt.subplot(222)
    plt.plot(pose[:,0]-t0, pose[:,4:7])
    plt.legend(['Roll','Pitch','Yaw'])
    plt.grid('on')
    plt.xlabel('Time (s)')
    plt.ylabel('Angle (rad)')
    plt.title('Attitude')

    if len(vel) > 0:
        plt.subplot(223)
        plt.plot(vel[:,0]-t0, vel[:,1:4])
        plt.legend(['Vx','Vy','Vz'])
        plt.grid('on')
        plt.xlabel('Time (s)')
        plt.ylabel('Velocity (m/s2)')
        plt.title('Velocity')

        plt.subplot(224)
        plt.plot(vel[:,0]-t0, vel[:,4:7])
        plt.legend(['Wx','Wy','Wz'])
        plt.grid('on')
        plt.xlabel('Time (s)')
        plt.ylabel('Ang. velocity (rad/s)')
        plt.title('Velocity')

    plt.tight_layout()
    return t0

def plot_odom(data, name, t0=None):
    if t0 is None:
        t0 = data[0,0]

    plt.figure(name)
    plt.subplot(311)
    plt.plot(data[:,0]-t0, data[:,1:4])
    plt.legend(['X','Y','Z'])
    plt.grid('on')
    plt.xlabel('Time (s)')
    plt.ylabel('Coordinates (m)')
    plt.title('Position')

    plt.subplot(312)
    plt.plot(data[:,0]-t0, data[:,7:10])
    plt.legend(['Vx','Vy','Vz'])
    plt.grid('on')
    plt.xlabel('Time (s)')
    plt.ylabel('Velocity (m/s2)')
    plt.title('Velocity')

    plt.subplot(313)
    plt.plot(data[:,0]-t0, data[:,10:13])
    plt.legend(['Wx','Wy','Wz'])
    plt.grid('on')
    plt.xlabel('Time (s)')
    plt.ylabel('Ang. velocity (rad/s)')
    plt.title('Angular velocity')
    plt.tight_layout()

    return t0

def plot_imu(data, name, t0=None):
    if t0 is None:
        t0 = data[0,0]

    plt.figure(name)
    plt.subplot(311)
    plt.plot(data[:,0]-t0, data[:,1:4])
    plt.legend(['Roll','Pitch','Yaw'])
    plt.grid('on')
    plt.xlabel('Time (s)')
    plt.ylabel('Angle (rad)')
    plt.title('Attitude')

    plt.subplot(312)
    plt.plot(data[:,0]-t0, data[:,4:7])
    plt.legend(['Wx','Wy','Wz'])
    plt.grid('on')
    plt.xlabel('Time (s)')
    plt.ylabel('Ang. velocity (rad/s)')
    plt.title('Gyro')

    plt.subplot(313)
    plt.plot(data[:,0]-t0, data[:,7:10])
    plt.legend(['Ax','Ay','Az'])
    plt.grid('on')
    plt.xlabel('Time (s)')
    plt.ylabel('Accel (m/s2)')
    plt.title('Accel')
    plt.tight_layout()

    return t0

def plot_traj(traj_dict, heading=None):
    plt.figure("Positions")
    plt.subplot(111)
    for src, traj in traj_dict.items():
        if traj is not None:
            plt.plot(traj[:,0],traj[:,1])
    if heading is not None:
        x = traj_dict['GPS'][:,0]
        y = np.tan(heading) * x
        plt.plot(x, y)
    plt.legend([k for k, v in traj_dict.items() if v is not None] + ['initial heading'])
    plt.grid('on')
    plt.xlabel('X (m)')
    plt.ylabel('Y (m)')
    plt.title("Positions")
    plt.axes().set_aspect('equal', 'datalim')
    plt.tight_layout()

def proj_gnss(data, origin, proj=None):
    gps_proj = np.empty((len(data), 4))
    for i in range(len(data)):
        if proj is None:
            x, y, z = data[i,1:4] - origin
        else:
            x, y, z = convert(data[i,1], data[i,2], data[i,3], origin, proj)
        gps_proj[i] = [data[i,0], x, y, z]
    return gps_proj

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Estimate initial heading and bias from bag')
    parser.add_argument('bag', type=str, help='ROS bag with initial data')
    parser.add_argument('--odom', '-o', type=str, nargs='?', const='/summit_xl/odom', help="plot Odometry data")
    parser.add_argument('--imu', '-i', type=str, nargs='?', const='/xsens/imu_data', help="plot IMU data")
    parser.add_argument('--gnss', '-g', type=str, nargs='?', const='/ublox/fix', help="plot GNSS data")
    parser.add_argument('--rtk', '-r', type=str, nargs='?', const='/ublox_rtk/fix', help="plot GNSS data")
    parser.add_argument('--pom', '-p', type=str, nargs='?', const='/pom/pose', help="plot POM estimate")
    parser.add_argument('--duration', '-d', type=float, help="take the first <duration> seconds of bag")
    parser.add_argument('--heading', type=float,
        help="initial heading (for Odometry rotation)", default=0)#1.2913964568)
    parser.add_argument('--reference', nargs='*', type=float)
    args = parser.parse_args()
    bag = args.bag
    t0 = None

    duration = None if (not args.duration) else args.duration

    if args.gnss:
        gps = parse_bag(bag, args.gnss, duration=duration)
        rtk = parse_bag(bag, args.rtk, duration=duration)
        if len(gps) == 0: gps = rtk
        if len(rtk) == 0: rtk = gps
        origin, proj = init_utm(
            lat=(args.reference[0] if args.reference else gps[0,1]),
            lon=(args.reference[1] if args.reference else gps[0,2]),
            alt=(args.reference[2] if args.reference else gps[0,3]))
        gps_proj = proj_gnss(gps, origin, proj)
        rtk_proj = proj_gnss(rtk, origin, proj)
        t0 = plot_gnss(gps_proj, rtk_proj, "GNSS", t0)

    if args.imu:
        imu = parse_bag(bag, args.imu, duration=duration)
        t0 = plot_imu(imu, 'XSens data', t0)

    if args.odom:
        odom = parse_bag(bag, args.odom, duration=duration)
        t0 = plot_odom(odom, 'Odometry', t0)
        c, s = np.cos(args.heading), np.sin(args.heading)
        R = np.array(((c,-s), (s, c)))
        odom[:,1:3] = odom[:,1:3].dot(R.transpose())
        odom_proj = proj_gnss(odom, odom[0,1:3])

    if args.pom:
        pom = {'pose': parse_bag(bag, args.pom, duration=duration),
            'vel': parse_bag(bag, "/pom/velocity", duration=duration)}
        plot_pom(pom['pose'], pom['vel'], "POM", t0)

    plot_traj({'Odometry': (odom_proj[:,1:3] if args.odom else None),
        'GPS': (gps_proj[:,1:3] if args.gnss else None),
        'RTK': (rtk_proj[:,1:3] if args.gnss else None),
        'POM': (pom['pose'][:,1:3] if args.pom else None),
        })
        #,heading=args.heading)

    plt.show()
