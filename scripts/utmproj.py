#!/usr/bin/env python

from pyproj import Proj
import utm
import numpy as np

def convert(lat, lon, alt, origin, proj):
	x, y = proj(lon, lat)
	return x - origin[0], y - origin[1], alt - origin[2]

def init_utm(lat=43.5704, lon=1.4723, alt=0):
	x, y, zone, zoneL = utm.from_latlon(lat, lon)
	origin = [x, y, alt]
	proj = Proj("+proj=utm +zone={}{} +ellps=WGS84 +datum=WGS84 +units=m +no_defs +x_0={} +y_0={}".format(zone, zoneL, origin[0], origin[1]))
	return origin, proj
