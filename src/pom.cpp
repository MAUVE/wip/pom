#include <iostream>

#include "pom/pom.hpp"
#include "pom/pom_conversions.hpp"

void POM::addMeasure(const std::string& data, or_pose_estimator_state_ptr& m) {
  if (this->restamp) ::restamp(m, &this->restamp_offset);
  this->measure._data[data.c_str()] = m;
}

void POM::request_stop() {
  this->stop_requested = true;
}

void POM::stop() {
  pom_io_stop(&ids, self);
}

bool POM::start() {
  auto s = pom_io_start(&ids, self);
  return (s == pom_read);
}

void POM::set_reference_frame(double x, double y, double z, double roll, double pitch, double yaw) {
  pom_set_position(x, y, z, roll, pitch, yaw, this->ids.context, &(this->ids.offset), this->self);
}

void POM::set_position(double x, double y, double z, double roll, double pitch, double yaw) {
  this->ids.context->state.p()[0] = x;
  this->ids.context->state.p()[1] = y;
  this->ids.context->state.p()[2] = z;
  Eigen::Quaternionf q = Eigen::AngleAxisf(roll, Eigen::Vector3f::UnitX())
    * Eigen::AngleAxisf(pitch, Eigen::Vector3f::UnitY())
    * Eigen::AngleAxisf(yaw, Eigen::Vector3f::UnitZ());
  this->ids.context->state.q()[0] = q.x();
  this->ids.context->state.q()[1] = q.y();
  this->ids.context->state.q()[2] = q.z();
  this->ids.context->state.q()[3] = q.w();
}

void POM::set_process_noise(double dadt, double dwdt) {
  ::set_process_noise(dadt, dwdt, self);
}

void POM::publishEstimate() {
  auto p = frame._data->pos._value;
  std::cout << "POM estimate: (" << p.x << ", " << p.y << ", " << p.z << ", "
    << p.qx << ", " << p.qy << ", " << p.qz << ", " << ", " << p.qw << ")" << std::endl;
}

void POM::io() {
  genom_event e;
  do {
    e = pom_io_read(&measure, &ids.ports, &ids.measurements, self);
    if (e == pom_insert) {
      history_mutex.lock();
      e = pom_io_insert(&(ids.measurements), &(ids.context), ids.history_length,
        &(ids.log_measurements), self);
      history_mutex.unlock();
      if (e == pom_error) {
        std::cerr << "[io] pom_io_insert error" << std::endl;
        return;
      }
    }
    else if (e == pom_error) {
      std::cerr << "[io] pom_io_read error" << std::endl;
      return;
    }
    usleep(pom_period_ms*1000);
  } while (e == pom_pause_read && !stop_requested);
}

void POM::filter() {
  genom_event e;
  e = pom_filter_start(& ids.context, &frame, self);
  if (e == pom_error) {
    std::cerr << "[filter] pom_filter_start: pom_e_sys" << std::endl;
    return;
  }
  // else exec
  do {
    history_mutex.lock();
    e = pom_filter_exec(& ids.context, pom_period_ms, &ids.offset, &ids.log_state, &frame, self);
    history_mutex.unlock();
    if (e == pom_error) {
      std::cerr << "[filter] pom_filter_exec: pom_e_sys" << std::endl;
      return;
    }
    publishEstimate();
    usleep(pom_period_ms*1000);
  } while (e == pom_pause_exec && !stop_requested);
}

POM::POM(bool restamp) : restamp(restamp), restamp_offset(nan("")) {
  frame._data = new or_pose_estimator_state;
  odomVelBias->vx = odomVelBias->vy = odomVelBias->vz = 0;
  odomVelBias->wx = odomVelBias->wy = odomVelBias->wz = 0;

  imuVelBias->vx = imuVelBias->vy = imuVelBias->vz = 0;
  imuVelBias->wx = imuVelBias->wy = imuVelBias->wz = 0;

  imuPoseBias->x = imuPoseBias->y = imuPoseBias->z = 0;
  imuPoseBias->qx = imuPoseBias->qy = imuPoseBias->qz = 0;
  imuPoseBias->qw = 1;

  gpsPoseBias->x = gpsPoseBias->y = gpsPoseBias->z = 0;
  gpsPoseBias->qx = gpsPoseBias->qy = gpsPoseBias->qz = 0;
  gpsPoseBias->qw = 1;

  imuAccBias->ax = imuAccBias->ay = imuAccBias->az = 0;

  odomCov->vx = odomCov->wz = 0.1 * 0.1;
  odomCov->vy = odomCov->vz = odomCov->wx = odomCov->wy = 0.05 * 0.05;
  gyroCov->vx = gyroCov->vy = gyroCov->vz = 0.0;
  gyroCov->wx = gyroCov->wy = gyroCov->wz = 0.01754;
  accCov->ax = accCov->ay = accCov->az = 4e-4;
}

void POM::add_imu(bool use_orientation, bool use_velocity, bool use_acceleration) {
  std::cout << "use IMU data" << std::endl;
  pom_add_measurement("imu", &ids.ports, &ids.measurements, self);
  measure._data["imu"] = nullptr;
  this->use_imu_orientation = use_orientation;
  this->use_imu_velocity = use_velocity;
  this->use_imu_acceleration = use_acceleration;
}

void POM::add_odom(bool use_linear, bool use_angular) {
  std::cout << "use Odometry data" << std::endl;
  pom_add_measurement("odom", &ids.ports, &ids.measurements, self);
  measure._data["odom"] = nullptr;
  this->use_odom_linear = use_linear;
  this->use_odom_angular = use_angular;
}

void POM::add_gps() {
  std::cout << "use GPS data" << std::endl;
  pom_add_measurement("gps", &ids.ports, &ids.measurements, self);
  measure._data["gps"] = nullptr;
}
