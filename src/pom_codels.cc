/*
 * Copyright (c) 2015-2018 LAAS/CNRS
 * All rights reserved.
 *
 * Redistribution and use  in source  and binary  forms,  with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of  source  code must retain the  above copyright
 *      notice and this list of conditions.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice and  this list of  conditions in the  documentation and/or
 *      other materials provided with the distribution.
 *
 *                                      Anthony Mallet on Mon Aug 24 2015
 */
#include "acpom.h"

#include <fcntl.h>
#include <unistd.h>

#include <cstdio>

#include <Eigen/Geometry>

#include "pom_c_types.h"
#include "codels.h"


/* --- Attribute set_process_noise -------------------------------------- */

/** Validation codel set_process_noise of attribute set_process_noise.
 *
 * Returns genom_ok.
 * Throws .
 */
genom_event
set_process_noise(double max_jerk, double max_dw,
                  const genom_context self)
{
  ukf::state_s::max_dadt = max_jerk;
  ukf::state_s::max_dwdt = max_dw;

  return genom_ok;
}


/* --- Function set_position -------------------------------------------- */

/** Codel pom_set_position of function set_position.
 *
 * Returns genom_ok.
 */
genom_event
pom_set_position(double x, double y, double z, double roll,
                 double pitch, double yaw,
                 const pom_context_s *context,
                 pom_ids_offset_s *offset, const genom_context self)
{
  Eigen::Map< Eigen::Matrix<double, 3, 1> > offp(offset->p);
  Eigen::Map< Eigen::Quaternion<double> > offq(offset->q);

  double cr = cos(roll/2.), cp = cos(pitch/2.), cy = cos(yaw/2.);
  double sr = sin(roll/2.), sp = sin(pitch/2.), sy = sin(yaw/2.);
  Eigen::Quaternion<double> q(
    cr * cp * cy + sr * sp * sy,
    sr * cp * cy - cr * sp * sy,
    cr * sp * cy + sr * cp * sy,
    cr * cp * sy - sr * sp * cy);

  offp =
    Eigen::Quaternion<double>(context->state.q()).conjugate()._transformVector(
      Eigen::Matrix<double, 3, 1>(x, y, z) - context->state.p());
  offq =
    q * Eigen::Quaternion<double>(context->state.q()).conjugate();

  return genom_ok;
}


/* --- Function log_state ----------------------------------------------- */

/** Codel pom_log_state of function log_state.
 *
 * Returns genom_ok.
 * Throws pom_e_sys.
 */
genom_event
pom_log_state(const char path[64], uint32_t decimation,
              pom_log_s **log_state, const genom_context self)
{
  int fd;

  fd = open(path, O_WRONLY|O_APPEND|O_CREAT|O_TRUNC, 0666);
  if (fd < 0) return pom_e_sys_error(path, self);

  if (write(fd, pom_log_header_fmt "\n", sizeof(pom_log_header_fmt)) < 0)
    return pom_e_sys_error(path, self);

  if ((*log_state)->req.aio_fildes >= 0) {
    close((*log_state)->req.aio_fildes);

    if ((*log_state)->pending)
      while (aio_error(&(*log_state)->req) == EINPROGRESS)
        /* empty body */;
  }
  (*log_state)->req.aio_fildes = fd;
  (*log_state)->pending = false;
  (*log_state)->skipped = false;
  (*log_state)->decimation = decimation < 1 ? 1 : decimation;
  (*log_state)->missed = 0;
  (*log_state)->total = 0;

  return genom_ok;
}


/* --- Function log_measurements ---------------------------------------- */

/** Codel pom_log_measurements of function log_measurements.
 *
 * Returns genom_ok.
 * Throws pom_e_sys.
 */
genom_event
pom_log_measurements(const char path[64], pom_log_s **log_measurements,
                     const genom_context self)
{
  int fd;

  fd = open(path, O_WRONLY|O_APPEND|O_CREAT|O_TRUNC, 0666);
  if (fd < 0) return pom_e_sys_error(path, self);

  if (write(fd, pom_log_header_fmt "\n", sizeof(pom_log_header_fmt)) < 0)
    return pom_e_sys_error(path, self);

  if ((*log_measurements)->req.aio_fildes >= 0) {
    close((*log_measurements)->req.aio_fildes);

    if ((*log_measurements)->pending)
      while (aio_error(&(*log_measurements)->req) == EINPROGRESS)
        /* empty body */;
  }
  (*log_measurements)->req.aio_fildes = fd;
  (*log_measurements)->pending = false;
  (*log_measurements)->skipped = false;
  (*log_measurements)->decimation = 1;
  (*log_measurements)->missed = 0;
  (*log_measurements)->total = 0;

  return genom_ok;
}


/* --- Function log_stop ------------------------------------------------ */

/** Codel pom_log_stop of function log_stop.
 *
 * Returns genom_ok.
 */
genom_event
pom_log_stop(pom_log_s **log_state, pom_log_s **log_measurements,
             const genom_context self)
{
  if (*log_state && (*log_state)->req.aio_fildes >= 0)
    close((*log_state)->req.aio_fildes);
  (*log_state)->req.aio_fildes = -1;

  if (*log_measurements && (*log_measurements)->req.aio_fildes >= 0)
    close((*log_measurements)->req.aio_fildes);
  (*log_measurements)->req.aio_fildes = -1;

  return genom_ok;
}


/* --- Function log_info ------------------------------------------------ */

/** Codel pom_log_info of function log_info.
 *
 * Returns genom_ok.
 */
genom_event
pom_log_info(const pom_log_s *log_state,
             const pom_log_s *log_measurements, double *state_miss,
             double *measurements_miss, const genom_context self)
{
  *state_miss =
    log_state->total ?
    100.0 * log_state->missed / log_state->total : 0.;
  *measurements_miss =
    log_measurements->total ?
    100.0 * (double)log_measurements->missed / log_measurements->total : 0.;

  return genom_ok;
}
