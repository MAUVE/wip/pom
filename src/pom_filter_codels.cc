/*
 * Copyright (c) 2015-2018 LAAS/CNRS
 * All rights reserved.
 *
 * Redistribution and use  in source  and binary  forms,  with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of  source  code must retain the  above copyright
 *      notice and this list of conditions.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice and  this list of  conditions in the  documentation and/or
 *      other materials provided with the distribution.
 *
 *                                      Anthony Mallet on Mon Aug 24 2015
 */
#include "acpom.h"

#include <sys/time.h>
#include <aio.h>
#include <err.h>
#include <unistd.h>

#include <cstdio>
#include <cmath>
#include <iostream>

#include <Eigen/Geometry>

#include "pom_c_types.h"

#include "codels.h"
#include "ukf.h"


/* --- Task filter ------------------------------------------------------ */

/** Codel pom_filter_start of task filter.
 *
 * Triggered by pom_start.
 * Yields to pom_exec.
 * Throws pom_e_sys.
 */
genom_event
pom_filter_start(pom_context_s **context, const pom_frame *frame,
                 const genom_context self)
{
  frame->open("robot", self);
  return pom_exec;
}


/** Codel pom_filter_exec of task filter.
 *
 * Triggered by pom_exec.
 * Yields to pom_pause_exec.
 * Throws pom_e_sys.
 */
genom_event
pom_filter_exec(pom_context_s **context, double pom_period_ms,
                const pom_ids_offset_s *offset, pom_log_s **log_state,
                const pom_frame *frame, const genom_context self)
{
  if (!(*context)) return pom_pause_exec;

  pom_history_s &h = (*context)->history;
  pom_history_s::reverse_iterator todo, skip, i, j;
  pom_history_s::iterator last;
  struct timeval start, now;
  double date, mdate;

  /* measure start time */
  gettimeofday(&start, NULL);

  /* get the oldest non fused state */
  for(todo = h.rbegin(); todo != h.rend(); todo++)
    if (!todo->second.fused) break;

  /* set filter state to the fused state before */
  if (todo == h.rend() || todo == h.rbegin()) {
    (*context)->filter.set((*context)->state, (*context)->cov);
    date = (*context)->ts.sec + 1e-9 * (*context)->ts.nsec;
  } else {
    todo--;
    (*context)->filter.set(todo->second.state, todo->second.cov);
    date = todo->first.sec + 1e-9 * todo->first.nsec;
    todo++;
  }

  /* process history */
  skip = h.rend();
  for(; todo != h.rend(); todo++) {
    pom_snapshot &s = todo->second;
    s.fused = false;

    /* if we've been computing for more than the period, skip to the
     * last measurement to be fused - skipped items will be processed next
     * time */
    if (skip == h.rend()) {
      gettimeofday(&now, NULL);
      double dt = (now.tv_sec - start.tv_sec) * 1e3 +
          (now.tv_usec - start.tv_usec) * 1e-3;
      if (dt > pom_period_ms) {
        for(i = j = todo, j++; j != h.rend(); i = j++) {
          if (i->second.fused && !j->second.fused) skip = i;
        }
        if (skip == h.rend()) {
          skip = h.rbegin();
        } else {
          todo = skip;
          (*context)->filter.set(todo->second.state, todo->second.cov);
          date = todo->first.sec + 1e-9 * todo->first.nsec;
          continue;
        }
      }
    }

    mdate = s.measurement.ts.sec + 1e-9 * s.measurement.ts.nsec;

    if (mdate <= date) {
      /*
       * skip measurements older than current state - this should not happen,
       * except for the very first measurement in the component lifetime.
       */
      (*context)->filter.predict(0.);
      (*context)->filter.update(s.state, s.cov);
      (*context)->filter.set(s.state, s.cov);
      s.fused = true;
      continue;
    }

    (*context)->filter.predict(mdate - date);

    /* position */
    if (s.measurement.pos._present &&
        !std::isnan(s.measurement.pos._value.x)) {
      (*context)->filter.measure.p() <<
        s.measurement.pos._value.x,
        s.measurement.pos._value.y,
        s.measurement.pos._value.z;

      if (s.measurement.pos_cov._present) {
        double *c = s.measurement.pos_cov._value.cov;

        (*context)->filter.measure.p_cov() <<
          c[0],  c[1],  c[3],
          c[1],  c[2],  c[4],
          c[3],  c[4],  c[5];
      } else {
        /* no covariance provided, provide a pessimistic default */
        (*context)->filter.measure.p_cov() <<
          1.,  0.,  0.,
          0.,  1.,  0.,
          0.,  0.,  1.;
      }
    }

    /* orientation */
    if (s.measurement.pos._present &&
        !std::isnan(s.measurement.pos._value.qx)) {
      (*context)->filter.measure.q() <<
        s.measurement.pos._value.qx,
        s.measurement.pos._value.qy,
        s.measurement.pos._value.qz,
        s.measurement.pos._value.qw;

      if (s.measurement.pos_cov._present) {
        double *c = s.measurement.pos_cov._value.cov;

        (*context)->filter.measure.q_cov() <<
          c[14], c[19], c[25], c[13],
          c[19], c[20], c[26], c[18],
          c[25], c[26], c[27], c[24],
          c[13], c[18], c[24],  c[9];
      } else {
        /* no covariance provided, provide a pessimistic default */
        double qx = s.measurement.pos._value.qx;
        double qy = s.measurement.pos._value.qy;
        double qz = s.measurement.pos._value.qz;
        double qw = s.measurement.pos._value.qw;

        (*context)->filter.measure.q_cov() <<
          0.25*(1-qx*qx), -0.25*qx*qy,     -0.25*qx*qz,     -0.25*qw*qx,
          -0.25*qx*qy,     0.25*(1-qy*qy), -0.25*qy*qz,     -0.25*qw*qy,
          -0.25*qx*qz,    -0.25*qy*qz,      0.25*(1-qz*qz), -0.25*qw*qz,
          -0.25*qw*qx,    -0.25*qw*qy,     -0.25*qw*qz,      0.25*(1-qw*qw);
      }
    }

    /* linear velocity */
    if (s.measurement.vel._present &&
        !std::isnan(s.measurement.vel._value.vx)) {
      (s.measurement.intrinsic ?
       (*context)->filter.measure.iv() :
       (*context)->filter.measure.v()) <<
        s.measurement.vel._value.vx,
        s.measurement.vel._value.vy,
        s.measurement.vel._value.vz;

      if (s.measurement.vel_cov._present) {
        double *c = s.measurement.vel_cov._value.cov;

        (s.measurement.intrinsic ?
         (*context)->filter.measure.iv_cov() :
         (*context)->filter.measure.v_cov()) <<
          c[0], c[1], c[3],
          c[1], c[2], c[4],
          c[3], c[4], c[5];
      } else {
        /* no covariance provided, provide a pessimistic default */
        (s.measurement.intrinsic ?
         (*context)->filter.measure.iv_cov() :
         (*context)->filter.measure.v_cov()) <<
          1., 0., 0.,
          0., 1., 0.,
          0., 0., 1.;
      }
    }

    /* angular velocity */
    if (s.measurement.vel._present &&
        !std::isnan(s.measurement.vel._value.wx)) {
      (s.measurement.intrinsic ?
       (*context)->filter.measure.iw() :
       (*context)->filter.measure.w()) <<
        s.measurement.vel._value.wx,
        s.measurement.vel._value.wy,
        s.measurement.vel._value.wz;

      if (s.measurement.vel_cov._present) {
        double *c = s.measurement.vel_cov._value.cov;

        (s.measurement.intrinsic ?
         (*context)->filter.measure.iw_cov() :
         (*context)->filter.measure.w_cov()) <<
          c[9],  c[13], c[18],
          c[13], c[14], c[19],
          c[18], c[19], c[20];
      } else {
        /* no covariance provided, provide a pessimistic default */
        (s.measurement.intrinsic ?
         (*context)->filter.measure.iw_cov() :
         (*context)->filter.measure.w_cov()) <<
          0.25,  0.,    0.,
          0.,    0.25,  0.,
          0.,    0.,    0.25;
      }
    }

    /* linear acceleration - XXX when intrisic, assume accelerometer */
    if (s.measurement.acc._present &&
        !std::isnan(s.measurement.acc._value.ax)) {
      (s.measurement.intrinsic ?
       (*context)->filter.measure.iawg() :
       (*context)->filter.measure.a()) <<
        s.measurement.acc._value.ax,
        s.measurement.acc._value.ay,
        s.measurement.acc._value.az;

      if (s.measurement.acc_cov._present) {
        double *c = s.measurement.acc_cov._value.cov;

        (s.measurement.intrinsic ?
         (*context)->filter.measure.iawg_cov() :
         (*context)->filter.measure.a_cov()) <<
          c[0], c[1], c[3],
          c[1], c[2], c[4],
          c[3], c[4], c[5];
      } else {
        /* no covariance provided, provide a pessimistic default */
        (s.measurement.intrinsic ?
         (*context)->filter.measure.iawg_cov() :
         (*context)->filter.measure.a_cov()) <<
          1., 0., 0.,
          0., 1., 0.,
          0., 0., 1.;
      }
    }

    (*context)->filter.update(s.state, s.cov);
    s.fused = true;

    /* update state for next iteration */
    (*context)->filter.set(s.state, s.cov);
    date = mdate;
  }

  //std::cout << "[pom_filter_exec] updated " << (*context)->state << std::endl;
  /* current state */
  gettimeofday(&now, NULL);
  (*context)->filter.predict(now.tv_sec + 1e-6*now.tv_usec - date);
  (*context)->filter.update((*context)->state, (*context)->cov);
  (*context)->ts.sec = now.tv_sec;
  (*context)->ts.nsec = now.tv_usec * 1000;
  //std::cout << "[pom_filter_exec] state " << (*context)->state << std::endl;

  /* publish */
  or_pose_estimator_state *r = frame->data("robot", self);
  if (r) {
    Eigen::Map<const Eigen::Matrix<double, 3, 1> > offp(offset->p);
    Eigen::Map<const Eigen::Quaternion<double> > offq(offset->q);

    r->ts = (*context)->ts;
    r->intrinsic = false;

    Eigen::Matrix<double, 3, 1> p =
      offq._transformVector((*context)->state.p()) + offp;
    Eigen::Quaternion<double> q =
      offq * Eigen::Quaternion<double>((*context)->state.q());
    r->pos._present = true;
    r->pos._value.x = p(0);
    r->pos._value.y = p(1);
    r->pos._value.z = p(2);
    r->pos._value.qw = q.w();
    r->pos._value.qx = q.x();
    r->pos._value.qy = q.y();
    r->pos._value.qz = q.z();
    {
      Eigen::Matrix<double, 7, 7> J;
      J.setZero();
      J.block<3, 3>(0, 0) = offq.matrix();
      J.block<3, 3>(3, 3) = offq.matrix();
      J(6, 6) = 1.;

      Eigen::Matrix<double, 7, 7> c =
        J * (*context)->state.pq_cov((*context)->cov) * J.transpose();

      r->pos_cov._present = true;
      for(uint8_t m = 0, k = 0; m < 7; m++)
        for(uint8_t n = 0; n <= m; n++, k++)
          r->pos_cov._value.cov[k] = c(m, n);
    }

    Eigen::Matrix<double, 3, 1> v =
      offq._transformVector((*context)->state.v());
    Eigen::Matrix<double, 3, 1> w =
      offq._transformVector((*context)->state.w());
    r->vel._present = true;
    r->vel._value.vx = v(0);
    r->vel._value.vy = v(1);
    r->vel._value.vz = v(2);
    r->vel._value.wx = w(0);
    r->vel._value.wy = w(1);
    r->vel._value.wz = w(2);
    {
      Eigen::Matrix<double, 6, 6> J;
      J.setZero();
      J.block<3, 3>(0, 0) = offq.matrix();
      J.block<3, 3>(3, 3) = offq.matrix();

      Eigen::Matrix<double, 6, 6> c =
        J * (*context)->state.vw_cov((*context)->cov) * J.transpose();

      r->vel_cov._present = true;
      for(uint8_t m = 0, k = 0; m < 6; m++)
        for(uint8_t n = 0; n <= m; n++, k++)
          r->vel_cov._value.cov[k] = c(m, n);
    }

    Eigen::Matrix<double, 3, 1> a =
      offq._transformVector((*context)->state.a());
    r->acc._present = true;
    r->acc._value.ax = a(0);
    r->acc._value.ay = a(1);
    r->acc._value.az = a(2);
    {
      Eigen::Matrix<double, 3, 3> J(offq.matrix());

      Eigen::Matrix<double, 3, 3> c =
        J * (*context)->state.a_cov((*context)->cov) * J.transpose();

      r->acc_cov._present = true;
      for(uint8_t m = 0, k = 0; m < 3; m++)
        for(uint8_t n = 0; n <= m; n++, k++)
          r->acc_cov._value.cov[k] = c(m, n);
    }
  }
  frame->write("robot", self);

  /* log */
  if ((*log_state)->req.aio_fildes >= 0) {
    (*log_state)->total++;
    if ((*log_state)->total % (*log_state)->decimation == 0) {
      if ((*log_state)->pending) {
        if (aio_error(&(*log_state)->req) != EINPROGRESS) {
          (*log_state)->pending = false;
          if (aio_return(&(*log_state)->req) <= 0) {
            warn("log_state");
            close((*log_state)->req.aio_fildes);
            (*log_state)->req.aio_fildes = -1;
          }
        } else {
          (*log_state)->skipped = true;
          (*log_state)->missed++;
        }
      }
    }

    if ((*log_state)->req.aio_fildes >= 0 && !(*log_state)->pending) {
      double qw = r->pos._value.qw;
      double qx = r->pos._value.qx;
      double qy = r->pos._value.qy;
      double qz = r->pos._value.qz;
      double roll, pitch, yaw;

      roll  = atan2(2 * (qw*qx + qy*qz), 1 - 2 * (qx*qx + qy*qy));
      pitch = asin(2 * (qw*qy - qz*qx));
      yaw   = atan2(2 * (qw*qz + qx*qy), 1 - 2 * (qy*qy + qz*qz));

      (*log_state)->req.aio_nbytes = snprintf(
        (*log_state)->buffer, sizeof((*log_state)->buffer),
        "%s" pom_log_state_fmt "\n",
        (*log_state)->skipped ? "\n" : "",
        r->ts.sec, r->ts.nsec,
        r->intrinsic,
        r->pos._present, r->vel._present, r->acc._present,
        r->pos._value.x, r->pos._value.y, r->pos._value.z,
        roll, pitch, yaw,
        r->vel._value.vx, r->vel._value.vy, r->vel._value.vz,
        r->vel._value.wx, r->vel._value.wy, r->vel._value.wz,
        r->acc._value.ax, r->acc._value.ay, r->acc._value.az);

      if (aio_write(&(*log_state)->req)) {
        warn("log_state");
        close((*log_state)->req.aio_fildes);
        (*log_state)->req.aio_fildes = -1;
      } else
        (*log_state)->pending = true;

      (*log_state)->skipped = false;
    }
  }

  return pom_pause_exec;
}
