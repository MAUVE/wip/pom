/*
 * Copyright (c) 2015-2018 LAAS/CNRS
 * All rights reserved.
 *
 * Redistribution and use  in source  and binary  forms,  with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of  source  code must retain the  above copyright
 *      notice and this list of conditions.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice and  this list of  conditions in the  documentation and/or
 *      other materials provided with the distribution.
 *
 *                                      Anthony Mallet on Mon Aug 24 2015
 */
#include "acpom.h"

#include <sys/time.h>
#include <aio.h>
#include <err.h>
#include <unistd.h>

#include <cassert>
#include <cmath>
#include <cstdio>

#include "pom_c_types.h"
#include "codels.h"


/* --- Task io ---------------------------------------------------------- */


/** Codel pom_io_start of task io.
 *
 * Triggered by pom_start.
 * Yields to pom_read.
 * Throws pom_e_sys.
 */
genom_event
pom_io_start(pom_ids *ids, const genom_context self)
{
  struct timeval now;
  pom_snapshot s;

  ids->context = new pom_context_s;
  ids->history_length = 2;

  /* set initial state */
  gettimeofday(&now, NULL);

  ids->context->ts.sec = now.tv_sec;
  ids->context->ts.nsec = now.tv_usec * 1000;

  ids->context->state = ukf::state_s::Identity();
  ids->context->cov = ukf::state_s::dcov_s::Identity();

  ids->max_jerk = ukf::state_s::max_dadt;
  ids->max_dw = ukf::state_s::max_dwdt;

  ids->offset.p[0] = ids->offset.p[1] = ids->offset.p[2] = 0.;
  ids->offset.q[0] = ids->offset.q[1] = ids->offset.q[2] = 0.;
  ids->offset.q[3] = 1.;

  /* init logging */
  try {
    ids->log_state = new pom_log_s;
    ids->log_measurements = new pom_log_s;
  } catch(...) {
    errno = ENOMEM;
    return pom_e_sys_error("init", self);
  }
  ids->log_state->req.aio_fildes = -1;
  ids->log_state->req.aio_offset = 0;
  ids->log_state->req.aio_buf = ids->log_state->buffer;
  ids->log_state->req.aio_nbytes = 0;
  ids->log_state->req.aio_reqprio = 0;
  ids->log_state->req.aio_sigevent.sigev_notify = SIGEV_NONE;
  ids->log_state->req.aio_lio_opcode = LIO_NOP;
  ids->log_state->pending = false;
  ids->log_state->skipped = false;

  ids->log_measurements->req.aio_fildes = -1;
  ids->log_measurements->req.aio_offset = 0;
  ids->log_measurements->req.aio_buf = ids->log_measurements->buffer;
  ids->log_measurements->req.aio_nbytes = 0;
  ids->log_measurements->req.aio_reqprio = 0;
  ids->log_measurements->req.aio_sigevent.sigev_notify = SIGEV_NONE;
  ids->log_measurements->req.aio_lio_opcode = LIO_NOP;
  ids->log_state->pending = false;
  ids->log_state->skipped = false;

  return pom_read;
}


/** Codel pom_io_read of task io.
 *
 * Triggered by pom_read.
 * Yields to pom_pause_read, pom_insert.
 * Throws pom_e_sys.
 */
genom_event
pom_io_read(const pom_measure *measure, sequence_pom_portinfo_s *ports,
            sequence_or_pose_estimator_state *measurements,
            const genom_context self)
{
  const or_pose_estimator_state *m;
  size_t i;

  for(i = 0; i < ports->_length; i++) {
    if (!ports->_buffer[i].name[0]) continue;

    /* get current port data */
    if (measure->read(ports->_buffer[i].name, self) != genom_ok) {
      ports->_buffer[i].name[0] = 0;
      continue;
    }
    m = measure->data(ports->_buffer[i].name, self);
    if (!m) continue;

    /* do not consider already read data */
    if (m->ts.nsec == ports->_buffer[i].last.nsec &&
        m->ts.sec == ports->_buffer[i].last.sec) continue;
    ports->_buffer[i].last = m->ts;

    /* record data */
    assert(measurements->_length < measurements->maximum());
    measurements->_buffer[measurements->_length] = *m;
    measurements->_length++;
  }

  return measurements->_length ? pom_insert : pom_pause_read;
}


/** Codel pom_io_insert of task io.
 *
 * Triggered by pom_insert.
 * Yields to pom_pause_read.
 * Throws pom_e_sys.
 */
genom_event
pom_io_insert(sequence_or_pose_estimator_state *measurements,
              pom_context_s **context, uint16_t history_length,
              pom_log_s **log_measurements, const genom_context self)
{
  or_pose_estimator_state *m;
  pom_snapshot s;
  struct timeval now;
  or_time_ts ts;
  ssize_t avail;
  size_t i, j;
  char *l;
  int n;

  /* check if logging is requested and possible */
  l = NULL;
  if ((*log_measurements)->req.aio_fildes >= 0) {
    if ((*log_measurements)->pending) {
      if (aio_error(&(*log_measurements)->req) != EINPROGRESS) {
        (*log_measurements)->pending = false;
        if (aio_return(&(*log_measurements)->req) <= 0) {
          warn("log_measurements");
          close((*log_measurements)->req.aio_fildes);
          (*log_measurements)->req.aio_fildes = -1;
        }
      } else {
        (*log_measurements)->skipped = true;
        (*log_measurements)->missed++;
      }
    }
    (*log_measurements)->total++;

    if ((*log_measurements)->req.aio_fildes >= 0 &&
        !(*log_measurements)->pending) {
      l = (*log_measurements)->buffer;
      avail = sizeof((*log_measurements)->buffer);

      if ((*log_measurements)->skipped) {
        n = snprintf(l, avail, "\n");
        l += n; avail -= n; assert(avail > 0 && "log buffer too small");
        (*log_measurements)->skipped = false;
      }
    }
  }

  /* insert new measurements with an empty state */
  s.fused = false;

  for(i = 0; i < measurements->_length; i++) {
    m = &measurements->_buffer[i];
    s.measurement = *m;
    (*context)->history.insert(pom_history_s::value_type(m->ts, s));

    /* build log buffer */
    if (l) {
      double qw = m->pos._value.qw;
      double qx = m->pos._value.qx;
      double qy = m->pos._value.qy;
      double qz = m->pos._value.qz;
      double roll, pitch, yaw;

      if (!m->pos._present) {
        m->pos._value.x = m->pos._value.y = m->pos._value.z = 0.;
        roll = pitch = yaw = 0.;
      } else {
        roll  = atan2(2 * (qw*qx + qy*qz), 1 - 2 * (qx*qx + qy*qy));
        pitch = asin(2 * (qw*qy - qz*qx));
        yaw   = atan2(2 * (qw*qz + qx*qy), 1 - 2 * (qy*qy + qz*qz));
      }
      if (!m->vel._present) {
        m->vel._value.vx = m->vel._value.vy = m->vel._value.vz = 0.;
        m->vel._value.wx = m->vel._value.wy = m->vel._value.wz = 0.;
      }
      if (!m->acc._present) {
        m->acc._value.ax = m->acc._value.ay = m->acc._value.az = 0.;
      }

      n = snprintf(
        l, avail, pom_log_state_fmt,
        m->ts.sec, m->ts.nsec,
        m->intrinsic,
        m->pos._present, m->vel._present, m->acc._present,
        m->pos._value.x, m->pos._value.y, m->pos._value.z,
        roll, pitch, yaw,
        m->vel._value.vx, m->vel._value.vy, m->vel._value.vz,
        m->vel._value.wx, m->vel._value.wy, m->vel._value.wz,
        m->acc._value.ax, m->acc._value.ay, m->acc._value.az);
      l += n; avail -= n; assert(avail > 0 && "log buffer too small");

      for(j = 0; j < 28; j++) {
        n = snprintf(l, avail, pom_logfmt, m->pos_cov._value.cov[j]);
        l += n; avail -= n; assert(avail > 0 && "log buffer too small");
      }
      for(j = 0; j < 21; j++) {
        n = snprintf(l, avail, pom_logfmt, m->vel_cov._value.cov[j]);
        l += n; avail -= n; assert(avail > 0 && "log buffer too small");
      }
      for(j = 0; j < 6; j++) {
        n = snprintf(l, avail, pom_logfmt, m->acc_cov._value.cov[j]);
        l += n; avail -= n; assert(avail > 0 && "log buffer too small");
      }
      n = snprintf(l, avail, "\n");
      l += n; avail -= n; assert(avail > 0 && "log buffer too small");
    }
  }
  measurements->_length = 0;

  /* truncate history */
  gettimeofday(&now, NULL);
  ts.sec = now.tv_sec - history_length;
  ts.nsec = now.tv_usec * 1000;

  (*context)->history.erase((*context)->history.upper_bound(ts),
                            (*context)->history.end());

  /* issue log request */
  if (l) {
    (*log_measurements)->req.aio_nbytes =
      sizeof((*log_measurements)->buffer) - avail;
    if (aio_write(&(*log_measurements)->req)) {
      warn("log_measurements");
      close((*log_measurements)->req.aio_fildes);
      (*log_measurements)->req.aio_fildes = -1;
    } else
      (*log_measurements)->pending = true;
  }

  return pom_pause_read;
}


/** Codel pom_io_stop of task io.
 *
 * Triggered by pom_stop.
 * Yields to pom_ether.
 * Throws pom_e_sys.
 */
genom_event
pom_io_stop(pom_ids *ids, const genom_context self)
{
  if (ids->context) delete ids->context;
  ids->context = NULL;

  pom_log_stop(&ids->log_state, &ids->log_measurements, self);
  delete ids->log_state;
  delete ids->log_measurements;
  ids->log_state = NULL;
  ids->log_measurements = NULL;

  return pom_ether;
}


/* --- Activity add_measurement ----------------------------------------- */

/** Codel pom_add_measurement of activity add_measurement.
 *
 * Triggered by pom_start.
 * Yields to pom_ether.
 * Throws pom_e_sys.
 */
genom_event
pom_add_measurement(const char port[128],
                    sequence_pom_portinfo_s *ports,
                    sequence_or_pose_estimator_state *measurements,
                    const genom_context self)
{
  size_t i;

  for(i = 0; i < ports->_length; i++) {
    if (!strcmp(ports->_buffer[i].name, port)) return pom_ether;
  }

  if (i >= ports->_length) {
    if (genom_sequence_reserve(measurements, i + 1))
      return pom_e_sys_error("add", self);
    if (genom_sequence_reserve(ports, i + 1))
      return pom_e_sys_error("add", self);

    ports->_length = i + 1;
  }

  strncpy(
    ports->_buffer[i].name, port, sizeof(ports->name));
  ports->_buffer[i].last.sec = 0;
  ports->_buffer[i].last.nsec = 0;

  return pom_ether;
}


/* --- Activity replay -------------------------------------------------- */

static FILE *replayf;
static double replayoff;

/** Codel pom_replay_start of activity replay.
 *
 * Triggered by pom_start.
 * Yields to pom_read, pom_ether.
 * Throws pom_e_sys.
 */
genom_event
pom_replay_start(const char path[128], const genom_context self)
{
  char header[1024];
  char *s;

  replayf = fopen(path, "r");
  if (!replayf) return pom_e_sys_error(path, self);

  /* discard header */
  s = fgets(header, sizeof(header), replayf);
  if (!s) return pom_stop;

  replayoff = nan("");

  return pom_read;
}

/** Codel pom_replay_read of activity replay.
 *
 * Triggered by pom_read.
 * Yields to pom_insert, pom_stop.
 * Throws pom_e_sys.
 */
genom_event
pom_replay_read(sequence_or_pose_estimator_state *measurements,
                const genom_context self)
{
  or_pose_estimator_state m;
  struct timeval now;
  struct timespec dt, rdt;
  int pp, pv, pa;
  double t;
  int i, c;

  do {
    /* get next measurement */
    double roll, pitch, yaw;

    c = fscanf(replayf, pom_scan_state_fmt,
               &m.ts.sec, &m.ts.nsec,
               &i, &pp, &pv, &pa,
               &m.pos._value.x, &m.pos._value.y, &m.pos._value.z,
               &roll, &pitch, &yaw,
               &m.vel._value.vx, &m.vel._value.vy, &m.vel._value.vz,
               &m.vel._value.wx, &m.vel._value.wy, &m.vel._value.wz,
               &m.acc._value.ax, &m.acc._value.ay, &m.acc._value.az);
    if (c == EOF) return pom_stop;
    else if (c != 21) {
      errno = EBADF;
      return pom_e_sys_error("replay", self);
    }

    m.intrinsic = i;
    m.pos._present = m.pos_cov._present = pp;
    m.vel._present = m.vel_cov._present = pv;
    m.acc._present = m.acc_cov._present = pa;

    double cr = cos(roll/2.), sr = sin(roll/2.);
    double cp = cos(pitch/2.), sp = sin(pitch/2.);
    double cy = cos(yaw/2.), sy = sin(yaw/2.);

    m.pos._value.qw = cy * cr * cp + sy * sr * sp;
    m.pos._value.qx = cy * sr * cp - sy * cr * sp;
    m.pos._value.qy = cy * cr * sp + sy * sr * cp;
    m.pos._value.qz = sy * cr * cp - cy * sr * sp;

    for(i = 0; i < 28; i++)
      if (!fscanf(replayf, "%lf", &m.pos_cov._value.cov[i])) return pom_stop;
    for(i = 0; i < 21; i++)
      if (!fscanf(replayf, "%lf", &m.vel_cov._value.cov[i])) return pom_stop;
    for(i = 0; i < 6; i++)
      if (!fscanf(replayf, "%lf", &m.acc_cov._value.cov[i])) return pom_stop;
    if (!fscanf(replayf, "\n"))
      /* tolerate missing newline, but consume it if present */;

    /* get time offset */
    gettimeofday(&now, NULL);
    if (std::isnan(replayoff)) {
      replayoff = now.tv_sec + 1e-6*now.tv_usec - m.ts.sec - 1e-9*m.ts.nsec;
    }

    t = m.ts.sec + 1e-9*m.ts.nsec + replayoff;
    m.ts.sec = floor(t);
    m.ts.nsec = 1e9*(t - m.ts.sec);

    /* get deadline */
    t = m.ts.sec + 1e-9*m.ts.nsec - now.tv_sec - 1e-6*now.tv_usec;

    /* record data */
    if (measurements->_length >= measurements->maximum())
      if (genom_sequence_reserve(measurements, 1 + measurements->maximum()))
        return pom_e_sys_error("replay", self);

    measurements->_buffer[measurements->_length] = m;
    measurements->_length++;
  } while(t < -1e-3);

  /* wait until deadline */
  if (t > 1e-3) {
    dt.tv_sec = floor(t);
    dt.tv_nsec = 1e9*(t - dt.tv_sec);
    while(nanosleep(&dt, &rdt)) {
      if (errno != EINTR) break;
      dt = rdt;
    }
  }

  return pom_insert;
}

/** Codel pom_io_insert of activity replay.
 *
 * Triggered by pom_insert.
 * Yields to pom_pause_read.
 * Throws pom_e_sys.
 */
/* already defined in task io */


/** Codel pom_replay_stop of activity replay.
 *
 * Triggered by pom_stop.
 * Yields to pom_ether.
 * Throws pom_e_sys.
 */
genom_event
pom_replay_stop(const genom_context self)
{
  if (replayf) {
    fclose(replayf);
    replayf = NULL;
  }

  return pom_ether;
}
