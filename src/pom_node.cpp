#include <ros/ros.h>
#include <ros/console.h>

#include "pom.hpp"
#include "pom_ros_conversions.hpp"

class POMNode : public POM {
private:
  ros::NodeHandle& nh;
  ros::Subscriber odom, imu, gps;
  ros::Publisher pose, vel;

protected:
    void fixCallback(const sensor_msgs::NavSatFix& fix) {
      std::shared_ptr<or_pose_estimator_state> m(new or_pose_estimator_state);
      GetNavSatFix(&fix, m, gpsPoseBias);
      addMeasure("gps", m);
    }
    void odomCallback(const nav_msgs::Odometry& odom) {
      std::shared_ptr<or_pose_estimator_state> m(new or_pose_estimator_state);
      GetOdometry(&odom, m, this->use_odom_linear, this->use_odom_angular, odomCov, odomVelBias);
      addMeasure("odom", m);
    };
    void imuCallback(const sensor_msgs::Imu& imu) {
      std::shared_ptr<or_pose_estimator_state> m(new or_pose_estimator_state);
      GetImu(&imu, m, this->use_imu_orientation, this->use_imu_velocity, this->use_imu_acceleration,
        /*imuPoseBias*/nullptr, imuVelBias, imuAccBias, gyroCov, accCov);
      addMeasure("imu", m);
    };

public:
  POMNode(ros::NodeHandle& n, bool restamp) : nh(n), POM(restamp) {
    std::string param;
    nh.param("use_imu_velocity", use_imu_velocity, true);
    nh.param("use_imu_orientation", use_imu_orientation, true);
    nh.param("use_imu_acceleration", use_imu_acceleration, true);
    nh.param("use_odom_linear", use_odom_linear, true);
    nh.param("use_odom_angular", use_odom_angular, true);
    nh.param("use_gps", use_gps, true);

    nh.param("period", this->pom_period_ms, 100.);
    std::cout << "POM period: " << this->pom_period_ms << std::endl;

    if (use_imu_acceleration || use_imu_orientation || use_imu_velocity) {
      this->imu = this->nh.subscribe("imu", 1000, &POMNode::imuCallback, this);
      add_imu(use_imu_orientation, use_imu_velocity, use_imu_acceleration);

      if (nh.searchParam("imu_att_bias", param)) {
        std::vector<float> b;
        nh.getParam(param, b);
        if (b.size() != 4) {
          std::cerr << "IMU attitude bias must contain 4D quaternion" << std::endl;
        }
        else {
          imuPoseBias = new or_t3d_pos;
          imuPoseBias->qx = b[0];
          imuPoseBias->qy = b[1];
          imuPoseBias->qz = b[2];
          imuPoseBias->qw = b[3];
        }
        std::cout << "IMU attitude bias: " << imuPoseBias << std::endl;
      }
      if (nh.searchParam("imu_vel_bias", param)) {
        std::vector<float> b;
        nh.getParam(param, b);
        if (b.size() != 3) {
          std::cerr << "IMU velocity bias must contain 3D angular vel." << std::endl;
        }
        else {
          imuVelBias = new or_t3d_vel;
          imuVelBias->vx = 0.0;
          imuVelBias->vy = 0.0;
          imuVelBias->vz = 0.0;
          imuVelBias->wx = b[0];
          imuVelBias->wy = b[1];
          imuVelBias->wz = b[2];
        }
        std::cout << "IMU velocity bias: " << imuVelBias << std::endl;
      }
      if (nh.searchParam("imu_acc_bias", param)) {
        std::vector<float> b;
        nh.getParam(param, b);
        if (b.size() != 3) {
          std::cerr << "IMU acceleration bias must contain 3D linear acc." << std::endl;
        }
        else {
          imuAccBias = new or_t3d_acc;
          imuAccBias->ax = b[0];
          imuAccBias->ay = b[1];
          imuAccBias->az = b[2];
        }
        std::cout << "IMU acceleration bias: " << imuAccBias << std::endl;
      }
      if (nh.searchParam("imu_vel_cov", param)) {
        std::vector<float> b;
        nh.getParam(param, b);
        if (b.size() != 6) {
          std::cerr << "IMU velocity covariance diagnonal: 6 values requested!" << std::endl;
        }
        else {
          gyroCov->vx = b[0];
          gyroCov->vy = b[1];
          gyroCov->vz = b[2];
          gyroCov->wx = b[3];
          gyroCov->wy = b[4];
          gyroCov->wz = b[5];
        }
      }
      if (nh.searchParam("imu_acc_cov", param)) {
        std::vector<float> b;
        nh.getParam(param, b);
        if (b.size() != 3) {
          std::cerr << "IMU acceleration covariance diagnonal: 3 values requested!" << std::endl;
        }
        else {
          accCov->ax = b[0];
          accCov->ay = b[1];
          accCov->az = b[2];
        }
      }
    }

    if (use_odom_linear || use_odom_angular) {
      this->odom = this->nh.subscribe("odom", 1000, &POMNode::odomCallback, this);
      add_odom(use_odom_linear, use_odom_angular);

      if (nh.searchParam("odom_bias", param)) {
        std::vector<float> b;
        nh.getParam(param, b);
        if (b.size() != 6) {
          std::cerr << "Odometry bias must contain 3D linear vel. + 3D angular vel." << std::endl;
        }
        else {
          odomVelBias->vx = b[0];
          odomVelBias->vy = b[1];
          odomVelBias->vz = b[2];
          odomVelBias->wx = b[3];
          odomVelBias->wy = b[4];
          odomVelBias->wz = b[5];
        }
        std::cout << "Odometry bias: " << odomVelBias << std::endl;
      }
      if (nh.searchParam("odom_cov", param)) {
        std::vector<float> b;
        nh.getParam(param, b);
        if (b.size() != 6) {
          std::cerr << "Odometry covariance diagnonal: 6 values requested!" << std::endl;
        }
        else {
          odomCov->vx = b[0];
          odomCov->vy = b[1];
          odomCov->vz = b[2];
          odomCov->wx = b[3];
          odomCov->wy = b[4];
          odomCov->wz = b[5];
        }
      }
    }

    if (use_gps) {
      this->gps = this->nh.subscribe("gps", 1000, &POMNode::fixCallback, this);
      add_gps();

      if (nh.searchParam("gps_bias", param)) {
        std::vector<float> b;
        nh.getParam(param, b);
        if (b.size() != 3) {
          std::cerr << "GPS bias is UTM initial position (x, y, z)" << std::endl;
        }
        else {
          gpsPoseBias = new or_t3d_pos;
          gpsPoseBias->x = b[0];
          gpsPoseBias->y = b[1];
          gpsPoseBias->z = b[2];
        }
        std::cout << "GPS pose bias: " << gpsPoseBias << std::endl;
      }
    }

    this->pose = this->nh.advertise<geometry_msgs::PoseStamped>("pose", 1000);
    this->vel = this->nh.advertise<geometry_msgs::TwistStamped>("velocity", 1000);
  };

  virtual void publishEstimate() override {
    geometry_msgs::PoseStamped p;
    GetPose(frame._data, p);
    this->pose.publish(p);
    geometry_msgs::TwistStamped v;
    GetTwist(frame._data, v);
    this->vel.publish(v);
  };

};

int main(int argc, char** argv) {
    ros::init(argc, argv, "pom");
    ros::NodeHandle n("~");
    POMNode pom(n, true);

    pom.start();

    std::string param;

    double heading = 0;
    if (n.searchParam("heading", param)) n.getParam(param, heading);
    std::cout << "Initial heading: " << heading << std::endl;

    if (n.searchParam("reference_frame", param)) {
      std::vector<float> ref;
      n.getParam(param, ref);
      if (ref.size() == 3)
        pom.set_reference_frame(ref[0], ref[1], ref[2], 0, 0, 0);
    }
    pom.set_position(0, 0, 0, 0, 0, heading);
    pom.set_process_noise(100, 50);

    std::thread io(&POMNode::io, &pom);
    std::thread filter(&POMNode::filter, &pom);

    ros::spin();

    pom.request_stop();
    io.join();
    filter.join();
    pom.stop();
    return 0;
}
