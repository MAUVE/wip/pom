#include <thread>
#include <iostream>
#include <mutex>

#include "codels.h"

const genom_context self;
std::mutex history_mutex;
double pom_period_ms = 10;


/*
codel<start> pom_replay_start(in path)
  yield read, ether;
async codel<read> pom_replay_read(out measurements)
  yield insert, stop;
codel<insert> pom_io_insert(inout measurements, inout context,
                            in history_length, inout log_measurements)
  yield pause::read;

codel<stop> pom_replay_stop()
  yield ether;
 */
void replay(const char path[128], pom_ids* ids)
{
  genom_event e;
  //e = pom_io_start(ids, self);
  // e == pom_read
  e = pom_replay_start(path, self);
  if (e == pom_ether) return;
  // else pom_read
  do {
    e = pom_replay_read(&(ids->measurements), self);
    or_pose_estimator_state* m = & ids->measurements._buffer[ids->measurements._length-1];
    printf("[replay] pom_replay_read: %d.09%d %f %f %f %f %f %f %f\n",
      m->ts.sec, m->ts.nsec, m->pos._value.x, m->pos._value.y,
      m->pos._value.z, m->pos._value.qx,m->pos._value.qy,
      m->pos._value.qz, m->pos._value.qw);
    if (e == pom_insert) {
      history_mutex.lock();
      e = pom_io_insert(&(ids->measurements), &(ids->context), ids->history_length,
        &(ids->log_measurements), self);
        history_mutex.unlock();
      if (e == pom_pause_read) {
        pom_snapshot* s = & (ids->context->history.begin())->second;
        or_pose_estimator_state* m = & s->measurement;
        printf("[replay] pom_io_insert: %d.09%d %f %f %f %f %f %f %f\n",
          m->ts.sec, m->ts.nsec, m->pos._value.x, m->pos._value.y,
          m->pos._value.z, m->pos._value.qx,m->pos._value.qy,
          m->pos._value.qz, m->pos._value.qw);
      } else {// e == pom_error
        std::cerr << "[replay] pom_io_insert: pom_e_sys" << std::endl;
        return;
      }
    } else if (e == pom_error) {
      std::cerr << "[replay] pom_replay_read: pom_e_sys" << std::endl;
      return;
    }
    usleep(pom_period_ms*1000);
  } while (e != pom_stop);
  e = pom_replay_stop(self);
  return;
}

/*
task filter {
  period period_ms ms;

  codel<start> pom_filter_start(out context, out frame) yield exec;
  codel<exec> pom_filter_exec(inout context, in offset, inout log_state,
                              out frame)
    yield pause::exec;

  throw e_sys;
};
*/
void filter(pom_context_s **context, const pom_frame *frame,
  const pom_ids_offset_s *offset, pom_log_s **log_state)
{
  genom_event e;
  e = pom_filter_start(context, frame, self);
  if (e == pom_error) {
    std::cerr << "[filter] pom_filter_start: pom_e_sys" << std::endl;
    return;
  }
  // else exec
  do {
    history_mutex.lock();
    e = pom_filter_exec(context, pom_period_ms, offset, log_state, frame, self);
    history_mutex.unlock();
    if (e == pom_error) {
      std::cerr << "[filter] pom_filter_exec: pom_e_sys" << std::endl;
      return;
    }
    printf("[filter] pom_filter_exec: %d.09%d %f %f %f %f %f %f %f\n",
      frame->_data->ts.sec, frame->_data->ts.nsec, frame->_data->pos._value.x,
      frame->_data->pos._value.y, frame->_data->pos._value.z, frame->_data->pos._value.qx,
      frame->_data->pos._value.qy, frame->_data->pos._value.qz, frame->_data->pos._value.qw
    );
    usleep(pom_period_ms*1000);
  } while (e == pom_pause_exec);
}

int main(int argc, char** argv) {

  pom_ids ids;
  pom_frame frame;
  frame._data = new or_pose_estimator_state;

  pom_io_start(&ids, self);
  set_process_noise(100, 50, self);
  pom_set_position(0, 0, 0, 0, 0, 0, ids.context, &ids.offset, self);

  //replay(argv[1], &ids);

  std::thread io_task (replay, argv[1], &ids);
  std::thread filter_task (filter, &(ids.context), &frame, &(ids.offset), &(ids.log_state));

  io_task.join();
  filter_task.join();

  return 0;
};
