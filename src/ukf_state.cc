/*
 * Copyright (c) 2015-2016 LAAS/CNRS
 * All rights reserved.
 *
 * Redistribution and use  in source  and binary  forms,  with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of  source  code must retain the  above copyright
 *      notice and this list of conditions.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice and  this list of  conditions in the  documentation and/or
 *      other materials provided with the distribution.
 *
 *                                      Anthony Mallet on Wed Aug 26 2015
 */

#include "ukf.h"

#include <Eigen/Geometry>

using namespace ukf;


/* --- state_s::transition ------------------------------------------------- */

state_s
state_s::transition(const state_s &in, double dt) {
  state_s out;

  /* acc, vel, pos */
  out.a() = in.a();
  out.v() = in.v() + in.a() * dt;
  out.p() = in.p() + in.v() * dt + 0.5 * in.a() * dt * dt;

  /* ang vel, att */
  out.w() = in.w();

  Eigen::Quaternion<double> omega_q;
  double a2 = dt * dt * in.w().squaredNorm();
  if (a2 < 1e-1) {
    omega_q.w() = 1 - a2/8 /*std::cos(a/2)*/;
    omega_q.vec() = (0.5 - a2/48 /*std::sin(a/2)/a*/) * dt * in.w();
  } else {
    double a = std::sqrt(a2);
    omega_q.w() = std::cos(a/2);
    omega_q.vec() = std::sin(a/2)/a * dt * in.w();
  }
  out.q() = (omega_q * Eigen::Quaternion<double>(in.q())).coeffs();
  return out;
}


/* --- state_s::process_noise ---------------------------------------------- */

double state_s::max_dadt = 200.;
double state_s::max_dwdt = 50.;

state_s::dcov_s
state_s::process_noise(double dt) const
{
  double astddev = dt*max_dadt/3.;
  double wstddev = dt*max_dwdt/3.;

  double avar = astddev * astddev;
  double wvar = wstddev * wstddev;

  double avar_dt = avar * dt;
  double avar_dt2 = avar_dt * dt;
  double avar_dt3 = avar_dt2 * dt;
  double avar_dt4 = avar_dt3 * dt;

  double wvar_dt = wvar * dt;
  double wvar_dt2 = wvar_dt * dt;

  dcov_s n = dcov_s::Zero();

  n.block<3,3>(0,0).diagonal() <<
    0.25 * avar_dt4, 0.25 * avar_dt4, 0.25 * avar_dt4;
  n.block<3,3>(6,0).diagonal() <<
    0.5 * avar_dt3, 0.5 * avar_dt3, 0.5 * avar_dt3;
  n.block<3,3>(0,6).diagonal() <<
    0.5 * avar_dt3, 0.5 * avar_dt3, 0.5 * avar_dt3;
  n.block<3,3>(12,0).diagonal() <<
    0.5 * avar_dt2, 0.5 * avar_dt2, 0.5 * avar_dt2;
  n.block<3,3>(0,12).diagonal() <<
    0.5 * avar_dt2, 0.5 * avar_dt2, 0.5 * avar_dt2;
  n.block<3,3>(6,6).diagonal() <<
    avar_dt2, avar_dt2, avar_dt2;
  n.block<3,3>(12,6).diagonal() <<
    avar_dt, avar_dt, avar_dt;
  n.block<3,3>(6,12).diagonal() <<
    avar_dt, avar_dt, avar_dt;
  n.block<3,3>(12,12).diagonal() <<
    avar, avar, avar;

  n.block<3,3>(3,3).diagonal() <<
    wvar_dt2, wvar_dt2, wvar_dt2;
  n.block<3,3>(9,3).diagonal() <<
    wvar_dt, wvar_dt, wvar_dt;
  n.block<3,3>(3,9).diagonal() <<
    wvar_dt, wvar_dt, wvar_dt;
  n.block<3,3>(9,9).diagonal() <<
    wvar, wvar, wvar;

  return n;
}


/* --- state_s::add_delta -------------------------------------------------- */

state_s
state_s::add_delta(const state_s &s, const delta_s &ds)
{
  state_s sds;

  Eigen::Matrix<double, 3, 1> dr = ds.segment<3>(3);
  double r2 = dr.squaredNorm();
  if (r2 < 1e-10)
    sds.q() = s.q();
  else {
    Eigen::Quaternion<double> dq;
    dq.w() = (1 - r2) / (1 + r2);
    dq.vec() = (1 + dq.w()) * dr;
    sds.q() = (dq * Eigen::Quaternion<double>(s.q())).coeffs();
  }


  sds.p() = s.p() + ds.segment<3>(0);
  sds.v() = s.v() + ds.segment<3>(6);
  sds.w() = s.w() + ds.segment<3>(9);
  sds.a() = s.a() + ds.segment<3>(12);

  return sds;
}


/* --- state_s::delta ------------------------------------------------------ */

state_s::delta_s
state_s::delta(const state_s &s2, const state_s &s1)
{
  delta_s ds;

  Eigen::Quaternion<double> dq =
    Eigen::Quaternion<double>(s2.q()) *
    Eigen::Quaternion<double>(s1.q()).conjugate();

  if (dq.w() >= 0)
    ds.segment<3>(3) = dq.vec()/(1.+dq.w());
  else
    ds.segment<3>(3) = -dq.vec()/(1.-dq.w());

  ds.segment<3>(0) = s2.p() - s1.p();
  ds.segment<3>(6) = s2.v() - s1.v();
  ds.segment<3>(9) = s2.w() - s1.w();
  ds.segment<3>(12) = s2.a() - s1.a();

  return ds;
}


/* --- state_s::q_cov ------------------------------------------------------ */

const Eigen::Matrix<double, 7, 7>
state_s::pq_cov(const dcov_s &cov) const
{
  Eigen::Matrix<double, 7, 6> J;
  double qx = q()(0), qy = q()(1), qz = q()(2), qw = std::fabs(q()(3));

  J.setZero();
  J.block<3, 3>(0, 0).setIdentity();
  J.block<4, 3>(3, 3) <<
    (1+qw) + qx*qx,           qx*qy,           qx*qz,
             qx*qy,  (1+qw) + qy*qy,           qy*qz,
             qx*qz,           qy*qz,  (1+qw) + qz*qz,
        -(1+qw)*qx,      -(1+qw)*qy,      -(1+qw)*qz;

  return J * cov.block<6, 6>(0, 0) * J.transpose();
}


/* --- measure_s::vector --------------------------------------------------- */

measure_s::vector_s
measure_s::vector() const
{
  vector_s m(sizem);
  int j = 0;

  if (pm.present) { m.segment<3>(j) = pm.data; j+=3; }
  if (qm.present) { m.segment<4>(j) = qm.data; j+=4; }
  if (vm.present) { m.segment<3>(j) = vm.data; j+=3; }
  if (wm.present) { m.segment<3>(j) = wm.data; j+=3; }
  if (am.present) { m.segment<3>(j) = am.data; j+=3; }
  if (ivm.present) { m.segment<3>(j) = ivm.data; j+=3; }
  if (iwm.present) { m.segment<3>(j) = iwm.data; j+=3; }
  if (iam.present) { m.segment<3>(j) = iam.data; j+=3; }
  if (iawgm.present) { m.segment<3>(j) = iawgm.data; j+=3; }

  return m;
}


/* --- measure_s::noise -------------------------------------------------- */

measure_s::cov_s
measure_s::noise() const
{
  cov_s n(dsizem, dsizem);
  int j = 0;

  n.setZero();
  if (pm.present) { n.block<3, 3>(j, j) = pm.cov; j+=3; }
  if (qm.present) {
    Eigen::Matrix<double, 3, 4> J;
    double qx = qm.data(0), qy = qm.data(1), qz = qm.data(2), qw = qm.data(3);
    double k = 1/(1 + std::fabs(qw));
    J <<
      k, 0, 0, -qx * k*k,
      0, k, 0, -qy * k*k,
      0, 0, k, -qz * k*k;

    n.block<3, 3>(j, j) = J * qm.cov * J.transpose();
    j += 3;
  }
  if (vm.present) { n.block<3, 3>(j, j) = vm.cov; j+=3; }
  if (wm.present) { n.block<3, 3>(j, j) = wm.cov; j+=3; }
  if (am.present) { n.block<3, 3>(j, j) = am.cov; j+=3; }
  if (ivm.present) { n.block<3, 3>(j, j) = ivm.cov; j+=3; }
  if (iwm.present) { n.block<3, 3>(j, j) = iwm.cov; j+=3; }
  if (iam.present) { n.block<3, 3>(j, j) = iam.cov; j+=3; }
  if (iawgm.present) { n.block<3, 3>(j, j) = iawgm.cov; j+=3; }

  return n;
}


/* --- measure_s::observe -------------------------------------------------- */

measure_s::vector_s
measure_s::observe(state_s in) const
{
  Eigen::Quaternion<double> qi =
    Eigen::Quaternion<double>(in.q()).conjugate();
  vector_s o(sizem);
  int j = 0;

  if (pm.present) { o.segment<3>(j) = in.p(); j+=3; }
  if (qm.present) { o.segment<4>(j) = in.q(); j+=4; }
  if (vm.present) { o.segment<3>(j) = in.v(); j+=3; }
  if (wm.present) { o.segment<3>(j) = in.w(); j+=3; }
  if (am.present) { o.segment<3>(j) = in.a(); j+=3; }
  if (ivm.present) { o.segment<3>(j) = qi._transformVector(in.v()); j+=3; }
  if (iwm.present) { o.segment<3>(j) = qi._transformVector(in.w()); j+=3; }
  if (iam.present) { o.segment<3>(j) = qi._transformVector(in.a()); j+=3; }
  if (iawgm.present) {
    o.segment<3>(j) =
      qi._transformVector(
        in.a() + Eigen::Matrix<double, 3, 1>(0, 0, 9.81));
    j+=3;
  }

  return o;
}


/* --- measure_s::delta ---------------------------------------------------- */

measure_s::vector_s
measure_s::delta(vector_s s2, vector_s s1) const
{
  vector_s d(dsizem);
  int j = 0, k = 0;

  if (pm.present) {
    d.segment<3>(j) = s2.segment<3>(k) - s1.segment<3>(k);
    j+=3; k+= 3;
  }
  if (qm.present) {
    Eigen::Quaternion<double> dq =
      Eigen::Quaternion<double>(s2.segment<4>(k)) *
    Eigen::Quaternion<double>(s1.segment<4>(k)).conjugate();

    if (dq.w() >= 0)
      d.segment<3>(j) = dq.vec()/(1.+dq.w());
    else
      d.segment<3>(j) = -dq.vec()/(1.-dq.w());

    j+=3; k+=4;
  }
  if (vm.present) {
    d.segment<3>(j) = s2.segment<3>(k) - s1.segment<3>(k);
    j+=3; k+= 3;
  }
  if (wm.present) {
    d.segment<3>(j) = s2.segment<3>(k) - s1.segment<3>(k);
    j+=3; k+= 3;
  }
  if (am.present) {
    d.segment<3>(j) = s2.segment<3>(k) - s1.segment<3>(k);
    j+=3; k+= 3;
  }
  if (ivm.present) {
    d.segment<3>(j) = s2.segment<3>(k) - s1.segment<3>(k);
    j+=3; k+= 3;
  }
  if (iwm.present) {
    d.segment<3>(j) = s2.segment<3>(k) - s1.segment<3>(k);
    j+=3; k+= 3;
  }
  if (iam.present) {
    d.segment<3>(j) = s2.segment<3>(k) - s1.segment<3>(k);
    j+=3; k+= 3;
  }
  if (iawgm.present) {
    d.segment<3>(j) = s2.segment<3>(k) - s1.segment<3>(k);
    j+=3; k+= 3;
  }

  return d;
}
